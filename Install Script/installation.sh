# install JSHint 
sudo npm install -g jshint
echo "JHint Installed"

# install python and pip 

apt install python-pip
apt install python3-pip
sudo apt-get install zip unzip

# npm install 
cd ../CodeServer 
npm install 
cd ../CodeServer Website
npm install 

# Install Python Vulture
cd ../CodeServer/vulturePythonAnalysis
sudo pip install .
cd ~

# install facebook infer
VERSION=0.XX.Y; \
curl -sSL "https://github.com/facebook/infer/releases/download/v$VERSION/infer-linux64-v$VERSION.tar.xz" \
| sudo tar -C /opt -xJ && \
ln -s "/opt/infer-linux64-v$VERSION/bin/infer" /usr/local/bin/infer

# install PMD 
cd $HOME
wget https://github.com/pmd/pmd/releases/download/pmd_releases%2F6.22.0/pmd-bin-6.22.0.zip
unzip pmd-bin-6.22.0.zip
alias pmd="$HOME/pmd-bin-6.22.0/bin/run.sh pmd"
