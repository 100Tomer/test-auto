# CodeService

This server meant for testing code! 
You can call its API to request a test or use the web UI.

## API

**GET** `/info/:language` - Get info of spasific language

**GET** `/exmaple/:language` - Get code example with spasific language

**GET** `/languageVersions/:language` - Get all possible language versions (For example for Python: 2.x, 3.x)

**POST** `/testCode` - Generating code from test cases

Body JSON Exmaple: 

```
{
    "language": "javascript",
    "testsRequests": [
        {
            "inputOfFunction": [
                "[1,2,3]",
                "2",
                "\"3\""
            ],
            "functionOutput": "5",
            "testName": "CheckIfShapeIsRectangle",
            "functionName": "CreateRectangle"
        },
        {
            "inputOfFunction": [
                "2332"
            ],
            "functionOutput": "true",
            "testName": "CircleTest",
            "functionName": "IsShapeIsCircle"
        }
    ]
}
```



**POST** `/runTests/TestCases` - Test code, with test case (that generated to code) 

Body JSON Exmaple: 

```
 {
     "language": "", // language can be auto detected but best practice it to send it
     "testCases": [
         {
             "inputOfFunction": [
                 ""
             ],
             "functionOutput": "\"Hello World\"",
             "testName": "Check If Return Is True",
             "functionName": "greet"
         }
     ],
     "codeToTest" : {
         "code": "function greet() { \n let a = 5 \n return \"hello world!\"; \n }"
     }
 }
```

**POST** `/` - Test code with test code, (For example java with unit testing)

```
{
    "code: "function greet() { \n return "hello world!"; \n }"
    "language: "javascript"
    "testCode: "describe(\"Testing function\", function() { \n   it(\"Is it a function?\", function() {\n        Test.assertEquals(typeof greet, \"function\", \"greet should be a function\"); \n   });\n   it(\"Correct return-value?\", function() { \n      Test.assertEquals(greet(), \"hello world!\");\n   });\n});"
    "languageVersion: "8.1.3/Babel" // optional
}
```

**POST** `/codecodeAnalysis` - Get code analysis warnnings. Exmaple: "unneeded variable"

```
{
    "language": "java",
    "code": "public class Main {
                public static String greet() {
                    return "hello world!";
                }
             }"
}
```


## Installation

#### compatible os 

Any OS that can run docker.

⚠️ You can't install docker on Windows Home because it doesn't include hyper-V 



### Step - 1
Do
`npm install`
for both server and client

### Step - 2

Install [Docker](https://www.docker.com/get-started).


### Step - 3

You can choose if you want to do this automatically or manually:

#### Automatically

Run the server side and it will automatically install the relevant docker images.

To Run, the server you can just skip to the 'Run' section.

⚠️ **It may take a few hours and it will not show any progress bar.**


#### manually

Pull the images manually:

`docker pull codewars/java-runner`

`docker pull codewars/node-runner`

`docker pull codewars/swift-runner`

`docker pull codewars/systems-runner`

`docker pull codewars/python-runner`

`docker pull codewars/dart-runner`

### Step - 4

This part is for the bug finder feature.

This step is installing [Facebook Infer](https://fbinfer.com/docs/getting-started):


### Step - 5

This is another code static analyzer
Install [PMD](https://pmd.github.io/)

### Step - 6

Install JSHint Globally:

` sudo npm install -g jshint`

### Step - 7

Install vulture - Python code analysis

Open terminal in the dictionary: vulturePythonAnalysis

than:

`sudo pip install .`

### Step - 8

Installtion of Coala: 

`docker pull coala/base`


run exmaple: 

`docker run -ti -v $(pwd):/app --workdir=/app coala/base coala --ci --files test.c --bears GNUIndentBear  --disable-caching`


# RUN 

## Run the server

1. Go to the folder "CodeServer" and execute:

`node server.js`

## Run the client

2. Go to the folder "CodeServer Website" and execute: 

`npm run server`


# Useful Information

## How to add support to a new language

1. create a new file in: CodeServer > Languages > languageInfo. For example: "Java.js". 
2. Fill the file with all the information needed (See other files to know what is required). (To find right dockerRunner go to [Here](https://github.com/Codewars/codewars-runner-cli) and search in the table)
3. After you filled all the data in the new file, add it to the array in languageManager.js. 
4. For here you can run tests with your languages.
5. (Optional - Part 1) To enable creating code from "Test Cases" to need to create creator file. For exmaple: CodeServer > testCreator > JavaScriptCreator.js. 
6. (Optional - Part 2) Than add it to the json in the file  CodeServer > testCreator > testCreator.js. 


## Supported Languages

1. Swift 
2. CPP
3. Java
4. Java Script
5. Python



## Custom Support 

This project is built upon [CodeWars project](https://hub.docker.com/r/codewars/codewars-runner/).

You can use that to add more languages that not included in their table.


# Todo List

1. Replace the code editor. Currently, he causes problem cause he not rerender like normal component. So currencly every change in the code or language cause :key change to make it rerender.
2. Test cases with ranges that generate random numbers. Like if the test case function input are ["hello", 2]. We will support ["hello", 2-10] and then choose number between 2-10. (Can be useful when wanna try a lot of different cases). Need to set rules for that and add it to the about page
3. Replace the About page with documentation. (One of them will be API, Test Case - Getting Started, and more).
4. Add image to vue server instead of calling it from git (Cause when the project is private - like now it won't work).


# FQA

1. Why there isn't API for multi-test requests? 

- The reason is mainly that some languages are so slow that running multi requests will not work well on a decent machine. (For example java)

2. Why we need the client-side? 

- It can be useful to test your own code, get code analysis to improve your coding, and also for generating test code without any sweat.


# License

Copyright © 2020 Tomer Cohen.

This project is MIT licensed.




