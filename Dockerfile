FROM node:lts-alpine as build-stage
WORKDIR ./CodeServerWebsite
COPY ./CodeServerWebsite ./
RUN pwd
RUN ls
RUN npm install -g @vue/cli
RUN npm install
RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage ./CodeServerWebsite/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]