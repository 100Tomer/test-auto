const FilesService = require('./FilesService');
const TestsService = require('./TestsService');
const express = require('express');
const app = express();
const port = 3000;
const bodyParser = require('body-parser');
const analyzer = require('./codeAnalizers/CodeAlayzerManager');
const testCreator = require('./testCreator/testCreator');
const codeTesterManager = require('./codeTester/codeTesterManager');
const dockerAuto = require('./dockerAuto');
const commandExecuter = require('./commandExecuter');
const languageManager = require('./Languages/languageManager');
require('custom-env').env(true);

const { detect } = require('program-language-detector');
const fs = require('fs');

app.use(function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*'); // update to match the domain you will make the request from
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	next();
});
app.use(
	bodyParser.urlencoded({
		extended: false
	})
);
app.use(bodyParser.json());

dockerAuto.runDocker();

const languageRunners = languageManager.allRunners();

const downloadAllDockerImages = async () => {
	for (let value in languageRunners) {
		let runnerName = languageRunners[value];

		let checkIfExistsOutput = await commandExecuter.execShellCommand(
			'docker images -q ' + runnerName
		);
		if (checkIfExistsOutput == '') {
			commandExecuter.execShellCommand('docker pull ' + runnerName);
			console.log(runnerName + ' - Downloading');
		} else {
			console.log(runnerName + ' - Exits');
		}
	}
};
downloadAllDockerImages();

app.get('/run/local/multiple/byCourse/:major/:course/:subject/:tar/:language', async (req, res) => {
	const files = await FilesService.fetchLocalFiles(
		req.params.major,
		req.params.course,
		req.params.subject,
		req.params.tar
	);
	let tests = [];
	const fileNames = Object.keys(files);
	Object.values(files).forEach((file, index) => {
		tests.push({
			studentNumber: fileNames[index]
				.split('.')[0]
				.substring(
					fileNames[index].split('.')[0].length - 3,
					fileNames[index].split('.')[0].length
				),
			// TODO: get the tests code from db or other file by the name of the tar given as parameter
			// Right now - base calc
			testCode: `import org.junit.Test;
import static org.junit.Assert.*;


public class MyTests {

    @Test
    public void A_numberToConvert_oneDigit() {
        assertEquals("The result is: 3 (in base 5) ",BaseCalculator.calc(new int[] {0, 3, 4, 5, 1})); 
    }

    @Test
    public void B_numberToConvert_numOfDigits() {
        assertEquals("The result is: 11002 (in base 3) ", BaseCalculator.calc(new int[] {0, 1232, 4, 3, 1})); 
    }
    @Test
    public void C_convertToBase_smallerBase() {
        assertEquals("The result is: 11 (in base 4) ", BaseCalculator.calc(new int[] {0, 10, 5, 4, 1})); 
    }
    @Test
    public void D_convertToBase_biggerBase() {
        assertEquals("The result is: 4 (in base 5) ", BaseCalculator.calc(new int[] {0, 10, 4, 5, 1})); 
    }
    @Test
    public void E_legalDestinationBase_destinationBaseEqualToMaxLimit() {
        assertEquals("The result is: 7 (in base 10) ", BaseCalculator.calc(new int[] {0, 12, 5, 10, 1})); 
    }

   @Test
    public void F_legalDestinationBase_destinationBaseEqualToMinLimit() {
        assertEquals("The result is: 111 (in base 2) ", BaseCalculator.calc(new int[] {0, 12, 5, 2, 1})); 
    }
    @Test
    public void G_legalOriginalBase_originalBaseEqualToMaxLimit() {
        assertEquals("The result is: 22 (in base 5) ", BaseCalculator.calc(new int[] {0, 12, 10, 5, 1})); 
    }

    @Test
    public void H_legalOriginalBase_equalToMinLimit() {
        assertEquals("The result is: 2 (in base 9) ", BaseCalculator.calc(new int[] {0, 10, 2, 9, 1})); 
    }
    @Test
    public void I_illegalDestinationBase_biggerThanMaxLimit	() {
        assertEquals("Invalid input: The base 12 is not between 2 to 10 ", BaseCalculator.calc(new int[] {0, 299, 10, 12, 1})); 
    }

    @Test
    public void J_illegalDestinationBase_smallerThanMinLimit	() {
        assertEquals("Invalid input: The base 1 is not between 2 to 10 ", BaseCalculator.calc(new int[] {0, 4, 6, 1, 1})); 
    }
    @Test
    public void K_illegalOriginalBase_smallerThanMinLimit() {
        assertEquals("Invalid input: The base 1 is not between 2 to 10 ", BaseCalculator.calc(new int[] {0, 2, 1, 1})); 
    }

    @Test
    public void L_illegalOriginalBase_biggerThanMaxLimit() {
        assertEquals("Invalid input: The base 12 is not between 2 to 10 ", BaseCalculator.calc(new int[] {0, 3, 12, 1})); 
    }
    @Test
    public void M_illegalNumberInBase_containsDigitsBiggerThanBase() {
        assertEquals("Invalid input: The number 1115111 is illegal in base: 3 ", BaseCalculator.calc(new int[] {0, 1115111, 3, 1})); 
    }
    @Test
    public void N_illegalNumberInBase_containsDigitsEqualToBase() {
        assertEquals("Invalid input: The number 1113111 is illegal in base: 3 ", BaseCalculator.calc(new int[] {0, 1113111, 3, 1})); 
    }
    @Test
    public void O_numberToConvert_negativeNumber() {
        assertEquals("Invalid input: The number to convert must be positive ", BaseCalculator.calc(new int[] {0, -12, 1})); 
    }

   @Test
    public void P_numberToConvert_zero() {
        assertEquals("The result is: 0 (in base 10) ", BaseCalculator.calc(new int[] {0, 0, 2, 10, 1})); 
    }
    @Test
    public void Q_convertMoreThanOneNumber_legalInputs() {
        assertEquals("The result is: 2 (in base 4) The result is: 2 (in base 4) ", BaseCalculator.calc(new int[] {0, 2, 3, 4, 0, 2, 3, 4, 1})); 
    }

    @Test
    public void R_convertMoreThanOneNumber_illegalInputs() {
        assertEquals("Invalid input: The base 300 is not between 2 to 10 The result is: 3 (in base 10) ", BaseCalculator.calc(new int[] {0, 10, 300, 0, 11, 2, 10, 1})); 
    }

    @Test
    public void S_convertMoreThanOneNumber_importanceOfResult() {
        assertEquals("The result is: 2 (in base 4) The result is: 101 (in base 2) ", BaseCalculator.calc(new int[] {0, 2, 3, 4, 0, 12, 3, 2, 1})); 
    }

    @Test
    public void T_programstops() {
        assertEquals("", BaseCalculator.calc(new int[] {1})); 
    }
}
`,
			language: req.params.language,
			code: file
		});
	});
	let results = [];
	if (tests.length > 0) {
		console.log('tests: ' + tests.length);
		console.log('files: ' + files.length);
		results = await TestsService.runMutipleTests(tests);
		res.send(results);
	} else {
		console.error('The requested files were not found ' + req.body);
		res.status(500).send('Error - Body is not json array');
	}
});

app.get(
	'/run/multiple/byCourse/:major/:course/:subject/:tar/:version/:language',
	async (req, res) => {
		const files = await FilesService.fetchFiles(
			req.params.major,
			req.params.course,
			req.params.subject,
			req.params.tar,
			req.params.version
		);
		console.log('files: ' + files);
		console.log('files values: ' + Object.values(files));
		let tests = [];
		const fileNames = Object.keys(files);
		Object.values(files).forEach((file, index) => {
			tests.push({
				studentNumber: fileNames[index]
					.split('.')[0]
					.substring(
						fileNames[index].split('.')[0].length - 3,
						fileNames[index].split('.')[0].length
					),
				// TODO: get the tests code from db or other file by the name of the tar given as parameter
				testCode: `import org.junit.Test;
      import static org.junit.Assert.*;
       
      public class MyTests {
      
          @Test
          public void oneDigit() {
              assertEquals(Main.calc(new int[] {0, 3, 4, 5, 1}), "The result is: 3 (in base 5) "); 
          }
      
          @Test
          public void numOfDigits() {
              assertEquals(Main.calc(new int[] {0, 1232, 4, 3, 1}), "The result is: 11002 (in base 3) "); 
          }
          @Test
          public void smallerBase() {
              assertEquals(Main.calc(new int[] {0, 10, 5, 4, 1}), "The result is: 11 (in base 4) "); 
          }
          @Test
          public void biggerBase() {
              assertEquals(Main.calc(new int[] {0, 10, 4, 5, 1}), "The result is: 4 (in base 5) "); 
          }
          @Test
          public void destBaseIs10() {
              assertEquals(Main.calc(new int[] {0, 12, 5, 10, 1}), "The result is: 7 (in base 10) "); 
          }
          @Test
          public void originalBaseIs10() {
              assertEquals(Main.calc(new int[] {0, 12, 10, 5, 1}), "The result is: 22 (in base 5) "); 
          }
          @Test
          public void illegalDestBase() {
              assertEquals(Main.calc(new int[] {0, 299, 10, 12, 1}), "Invalid input: The base 12 is not between 2 to 10 "); 
          }
          @Test
          public void illegalOriginalBase() {
              assertEquals(Main.calc(new int[] {0, 2, 1, 1}), "Invalid input: The base 1 is not between 2 to 10 "); 
          }
          @Test
          public void illegalNumberInBase_five() {
              assertEquals(Main.calc(new int[] {0, 1115111, 3, 1}), "Invalid input: The number 1115111 is illegal in base: 3 "); 
          }
          @Test
          public void illegalNumberInBase_three() {
              assertEquals(Main.calc(new int[] {0, 1113111, 3, 1}), "Invalid input: The number 1113111 is illegal in base: 3 "); 
          }
          @Test
          public void numberToConvertNegatuve() {
              assertEquals(Main.calc(new int[] {0, -12, 1}), "Invalid input: The number to convert must be positive "); 
          }
          @Test
          public void convertMoreThanOneNumber() {
              assertEquals(Main.calc(new int[] {0, 2, 3, 4, 0, 2, 3, 4, 1}), "The result is: 2 (in base 4) The result is: 2 (in base 4) "); 
          }
          @Test
          public void convertMoreThanOneNumber_importanceOfResult() {
              assertEquals(Main.calc(new int[] {0, 2, 3, 4, 0, 12, 3, 2, 1}), "The result is: 2 (in base 4) The result is: 101 (in base 2) "); 
          }
          @Test
          public void programstops() {
              assertEquals(Main.calc(new int[] {1}), ""); 
          }
      
      }
      `,
				language: req.params.language,
				code: file
			});
		});
		let results = [];

		if (tests.length > 0) {
			console.log('tests: ' + tests.length);
			console.log('files: ' + files.length);
			results = await TestsService.runMutipleTests(tests);
			res.send(results);
		} else {
			console.error('The requested files were not found ' + req.body);
			res.status(500).send('Error - Body is not json array');
		}
	}
);

app.get(
	'/run/multiple/byFileName/:major/:course/:subject/:tar/:version/:language/:fileName',
	async (req, res) => {
		const files = await FilesService.fetchFilesByFileName(
			req.params.major,
			req.params.course,
			req.params.subject,
			req.params.tar,
			req.params.version,
			req.params.fileName
		);
		let tests = [];
		const fileNames = Object.keys(files);
		Object.values(files).forEach((file, index) => {
			tests.push({
				studentNumber: fileNames[index]
					.split('.')[0]
					.substring(
						fileNames[index].split('.')[0].length - 3,
						fileNames[index].split('.')[0].length
					),
				// TODO: get the tests code from db or other file by the name of the tar given as parameter
				testCode: `import org.junit.Test;
      import static org.junit.Assert.*;
       
      public class MyTests {
      
          @Test
          public void oneDigit() {
              assertEquals(Main.calc(new int[] {0, 3, 4, 5, 1}), "The result is: 3 (in base 5) "); 
          }
      
          @Test
          public void numOfDigits() {
              assertEquals(Main.calc(new int[] {0, 1232, 4, 3, 1}), "The result is: 11002 (in base 3) "); 
          }
          @Test
          public void smallerBase() {
              assertEquals(Main.calc(new int[] {0, 10, 5, 4, 1}), "The result is: 11 (in base 4) "); 
          }
          @Test
          public void biggerBase() {
              assertEquals(Main.calc(new int[] {0, 10, 4, 5, 1}), "The result is: 4 (in base 5) "); 
          }
          @Test
          public void destBaseIs10() {
              assertEquals(Main.calc(new int[] {0, 12, 5, 10, 1}), "The result is: 7 (in base 10) "); 
          }
          @Test
          public void originalBaseIs10() {
              assertEquals(Main.calc(new int[] {0, 12, 10, 5, 1}), "The result is: 22 (in base 5) "); 
          }
          @Test
          public void illegalDestBase() {
              assertEquals(Main.calc(new int[] {0, 299, 10, 12, 1}), "Invalid input: The base 12 is not between 2 to 10 "); 
          }
          @Test
          public void illegalOriginalBase() {
              assertEquals(Main.calc(new int[] {0, 2, 1, 1}), "Invalid input: The base 1 is not between 2 to 10 "); 
          }
          @Test
          public void illegalNumberInBase_five() {
              assertEquals(Main.calc(new int[] {0, 1115111, 3, 1}), "Invalid input: The number 1115111 is illegal in base: 3 "); 
          }
          @Test
          public void illegalNumberInBase_three() {
              assertEquals(Main.calc(new int[] {0, 1113111, 3, 1}), "Invalid input: The number 1113111 is illegal in base: 3 "); 
          }
          @Test
          public void numberToConvertNegatuve() {
              assertEquals(Main.calc(new int[] {0, -12, 1}), "Invalid input: The number to convert must be positive "); 
          }
          @Test
          public void convertMoreThanOneNumber() {
              assertEquals(Main.calc(new int[] {0, 2, 3, 4, 0, 2, 3, 4, 1}), "The result is: 2 (in base 4) The result is: 2 (in base 4) "); 
          }
          @Test
          public void convertMoreThanOneNumber_importanceOfResult() {
              assertEquals(Main.calc(new int[] {0, 2, 3, 4, 0, 12, 3, 2, 1}), "The result is: 2 (in base 4) The result is: 101 (in base 2) "); 
          }
          @Test
          public void programstops() {
              assertEquals(Main.calc(new int[] {1}), ""); 
          }
      
      }
      `,
				language: req.params.language,
				code: file
			});
		});
		let results = [];

		if (tests.length > 0) {
			results = await TestsService.runMutipleTests(tests);
			res.send(results);
		} else {
			console.error('The requested files were not found ' + req.body);
			res.status(500).send('Error - Body is not json array');
		}
	}
);

app.post('/', async (req, res) => {
	let results = [];
	if (req.body instanceof Array) {
		let waitingTests = req.body.length;
		results = await TestsService.runMutipleTests(req.body);
	} else {
		console.error('Body is not json array ' + req.body);
		res.status(500).send('Error - Body is not json array');
	}

	res.send(results);
});

app.post('/runTests/TestCases', async (req, res) => {
	console.log(req.body);
	const testCodeRequest = req.body['testCases'];
	let language = req.body['language'];
	const languageVersion = req.body['languageVersion'];
	const test = req.body['codeToTest'];
	if (language == null || language == '') {
		try {
			language = detect(test.code).toLowerCase();
		} catch (error) {
			console.log(error);
		}
	}

	if (language != null && language != '' && language != 'unknown') {
		let generaedTest = testCreator.createTestCode(testCodeRequest, language);
		if (generaedTest != '') {
			if (language && test.code) {
				let results = await codeTesterManager.testCode(
					test.code,
					generaedTest,
					language,
					languageVersion
				);
				console.log('Code Testser Results: ' + results);
				res.send(results);
			} else {
				console.error('Missing body arguments: ' + req.body);
				res.status(500).send('Error - Body missing some arguments');
			}
		} else {
			res.status(500).send('Error - Data is not valid or launguage is not supported');
		}
	} else {
		res.status(500).send('Error - launguage didnt sent and could not be detected');
	}
});

app.post('/codeAnalysis', async (req, res) => {
	const code = req.body['code'];
	const language = req.body['language'];
	if (code != null && code != '' && language != null && language != '') {
		let analizedCodeResults = await analyzer.AnalyzeCode(code, language);
		res.send(analizedCodeResults);
	} else {
		res.status(500).send('Error - Code Or language not exist in the request body');
	}
});

app.get('/languageVersions/:language', async (req, res) => {
	const language = req.params['language'];

	try {
		const versions = languageManager.languageVersionsOf(language);
		return res.send(versions);
	} catch (error) {
		res.status(500).send('Error - language or language versions not exists ');
	}
});

app.get('/languages', async (req, res) => {
	try {
		const versions = languageManager.AllLanguageVersions();
		console.log(versions);
		return res.send(versions);
	} catch (error) {
		res.status(500).send('Error - language or language versions not exists ');
	}
});

app.get('/example/:language', async (req, res) => {
	const language = req.params['language'];
	try {
		const codeExample = languageManager.codeExampleOf(language.toLowerCase());

		res.send(codeExample);
	} catch (error) {
		res.status(500).send('Error - language example not exists ');
	}
});

app.get('/info/:language', async (req, res) => {
	const language = req.params['language'];
	try {
		const codeData = languageManager.getDataOfLangauge(language.toLowerCase());
		res.send(codeData);
	} catch (error) {
		res.status(500).send('Error - language data not exists ');
	}
});

app.get('/languages/iconNames/', async (req, res) => {
	try {
		let allLanguagesIconName = languageManager.allLanguagesIconsOf();
		res.send(allLanguagesIconName);
	} catch (error) {
		res.status(500).send('Error - language info not exists');
	}
});

app.get('/files/course/:major/:course/:subject/:tar/:version', async (req, res) => {
	const files = await FilesService.fetchFiles(
		req.params.major,
		req.params.course,
		req.params.subject,
		req.params.tar,
		req.params.version
	);
	res.json(files);
});

app.post('/testCode', async (req, res) => {
	const testCodeRequest = req.body;
	console.log(req.body);
	let language = testCodeRequest['language'].toLowerCase();
	let code = testCreator.createTestCode(testCodeRequest['testsRequests'], language);
	console.log('Test Generated Code: ' + code);
	if (code != '') {
		res.send(code);
	} else {
		res.error('Error - Data is not valid or launguage is not supported');
	}
});

app.listen(port, () => {
	console.log(`Example app listening on port ${port}!`);
});

function replaceAll(str, find, replace) {
	return str.replace(new RegExp(find, 'g'), replace);
}
