const fixCode = (code) => {
    let codeAfterFix = "";
    const testCodeLines = code.split('\n')
    codeAfterFix = testCodeLines.map(currentLine => {
        if (currentLine.includes('test.assert_equals') || currentLine.includes('assert_approx_equals') || currentLine.includes('expect_error') || currentLine.includes('assert_not_equals')) {
            let LineWithoutWhiteSpaces = currentLine.replace(/^\s+/g, '');
            let onlyWhiteSpaces = currentLine.replace(LineWithoutWhiteSpaces, '');

            return onlyWhiteSpaces + "try:" + "\n" + "    " + currentLine + "\n" + onlyWhiteSpaces + "except:\n" + onlyWhiteSpaces + "    pass";
        } else {
            return currentLine
        }
    }).join('\n')
    return codeAfterFix
}


exports.fixCode = fixCode