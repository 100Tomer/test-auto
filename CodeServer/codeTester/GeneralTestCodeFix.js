const fixCode = (testCode) => {
    let codeAfterFix = replaceAll(testCode, "\"", "\\\"");
    return codeAfterFix
}


exports.fixCode = fixCode

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}