const fixCode = (code) => {
    let codeAfterFix = code;
    codeAfterFix = codeMainFunctionFixing(codeAfterFix);
    codeAfterFix = replaceAll(codeAfterFix, "\"", "\\\"");
    // codeAfterFix = replaceAll(codeAfterFix, "'", "\\'");

    return codeAfterFix
}


exports.fixCode = fixCode

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}


const codeMainFunctionFixing = (code) => {

    //const regex = /(void main\()|(int main\()/gm;
    if (code.includes("void main(")) {
        return code.replace("void main(", "void main2(");
    }

    if (code.includes("int main(")) {
        return code.replace("int main(", "int main2(");
    }
    return code;
};