const PythonSpecialCodeChnage = require('./SpecialCodeChnages/PythonSpecialCodeChnage')
const GeneralCodeFix = require('./GeneralCodeFix')
const GeneralTestCodeFix = require('./GeneralTestCodeFix')
const analyzer = require('../codeAnalizers/CodeAlayzerManager');
const commandExecuter = require("../commandExecuter")
const languageManager = require("../Languages/languageManager")
const DartSpecialCodeFix = require("./SpecialCodeChnages/DartSpecialCodeFix")

const languagesTestCodeSpecialFix = {
    "python": PythonSpecialCodeChnage,

}
const languagesCodeSpecialFix = {

    // probebly not needed
    // "dart": DartSpecialCodeFix,
}

const testCode = async (code, testCode, language, languageVersion) => {

    let fixedCode = GeneralCodeFix.fixCode(code);

    if (languagesCodeSpecialFix[language] != null) {

        fixedCode = languagesCodeSpecialFix[language].fixCode(fixedCode)
    }


    let fixedTestCode = testCode;

    if (languagesTestCodeSpecialFix[language] != null) {

        fixedTestCode = languagesTestCodeSpecialFix[language].fixCode(testCode)
    }

    fixedTestCode = GeneralTestCodeFix.fixCode(fixedTestCode);


    let finalLanguageVersion = languageVersion;
    if (finalLanguageVersion == null) {
        const optionalVersions = languageManager.languageVersionsOf(language);
        finalLanguageVersion = optionalVersions[optionalVersions.length - 1];
    }

    const codeRunner = languageManager.RunnerOf(language);

    const bashCommand = 'docker run --rm ' + codeRunner + ' run -l ' + language + " --languageVersion " + finalLanguageVersion + ' -d -c "' + fixedCode + '" -t ' + languageManager.languageTestRuunerOf(language) + ' -f "' + fixedTestCode + '"   ';
    console.log("Command: " + bashCommand)
    const output = await commandExecuter.execShellCommand(bashCommand, null, ["Unhandled rejection TestError:", "AssertException"]);

    //let analizedCodeResults = await analyzer.AnalyzeCode(code, language);
    let analizedCodeResults = '';
    console.info("Analized Results: " + analizedCodeResults);


    let results = parseOutput(output, analizedCodeResults, language);
    return results
}

exports.testCode = testCode


const parseOutput = (output, analizedCodeResults, language) => {
    
    let results = [];
    let allResults = [];
    if (output.includes("Execute Error")) {

        allResults.push({
            "isSyntaxError": false,
            "isExecuteError": true,
            "errorDecription": output
        });
    } else if (output.includes("Syntax Error")) {
        allResults.push({
            "isExecuteError": false,
            "isSyntaxError": true,
            "syntaxErrorDescription": output,
            "codeAnalysis": analizedCodeResults
        });
    } else {

        // Getting the speed of request
        const onlyLineWithMS = output.split('\n').filter(function (s) {
            return s.includes('ms');
        })
        let runTime = onlyLineWithMS[0].replace('ms', '');

        // fixing the sdout to understandable json
        const fixedOut = output.split('\n').filter(function (s) {
            return s.includes('<FAILED::>') || s.includes('<PASSED::>') || s.includes('<LOG::-Exception Details>');
        });

        let testDescriptions = [];
        let existTestDescriptions = output.split('\n').filter(function (s) {
            return s.includes('<DESCRIBE::>');
        });

        let existTestDescriptionsIt = output.split('\n').filter(function (s) {
            return s.includes('<IT::>');
        });

        existTestDescriptionsIt > existTestDescriptions ? testDescriptions = existTestDescriptionsIt : testDescriptions = existTestDescriptions;
        
        let resultIndex = 0;

        fixedOut.forEach((element, index) => {
            if(element.includes('<LOG::-Exception Details>')){
                let isRuntimeError = false;
                let exceptionDetails = '';
                try {
                    exceptionDetails = element.split("<LOG::-Exception Details>")[1].split(":")[0];
                    if(exceptionDetails !== "org.junit.ComparisonFailure"){
                        isRuntimeError = true;
                    }
                } catch(err) {
                    isRuntimeError = false;
                    exceptionDetails = '';
                }
                
                results[resultIndex].isRuntimeError = isRuntimeError;
                results[resultIndex].exceptionDetails = exceptionDetails;
                resultIndex++;
            } else {

                let elementWithoutTag = element;
                if (elementWithoutTag) {
                    elementWithoutTag = element.replace('<PASSED::>', '').replace('<FAILED::>', '');
                }
                let testDescriptionsWithoutTag = testDescriptions[resultIndex];
                if (testDescriptionsWithoutTag) {
                    testDescriptionsWithoutTag = testDescriptions[resultIndex].replace('<DESCRIBE::>', '').replace('<IT::>', '');
                }

                let isTestPassed = null;

                // The result index is not being incremented because the next element is a log that is relevant to this error
                if (element.includes('<FAILED::>')) {
                    isTestPassed = false;
                }
                if (element.includes('<PASSED::>')) {
                    isTestPassed = true;
                    resultIndex++;
                }

                

                let actual = '';
                let expected = '';

                if(!isTestPassed) {
                    let tempResult;
                    try {
                        switch (language) {
                            case 'java':
                                tempResult = elementWithoutTag.split('expected:')[1].split(' but was:');
                                tempResult = tempResult.map(cell=>cell.replace('<','').replace('>','').replace('[','').replace(']',''));
                                expected = tempResult[0]
                                actual = tempResult[1];
                                break;
                            case 'javascript':
                                tempResult = elementWithoutTag.split('Expected: ')[1].split(', instead got: ');
                                tempResult = tempResult.map(cell=>cell.replace(/'/g,``));
                                expected = tempResult[0]
                                actual = tempResult[1];
                                break;
                            default:
                                break;
                        }
                    } catch(err) {
                        expected = "";
                        actual = "";
                    }
                }

                results.push({
                    "testDescription": testDescriptionsWithoutTag,
                    "result": elementWithoutTag,
                    "expected": expected,
                    "actual": actual,
                    "isTestPassed": isTestPassed,
                    "isSyntaxError": false
                });
            }   
        });
        allResults.push({
            "isExecuteError": false,
            "testResults": results,
            "codeAnalysis": analizedCodeResults,
            "runTime": runTime
        });
    }
    return allResults
}