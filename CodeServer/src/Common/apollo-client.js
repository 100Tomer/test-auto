const fetch = require('node-fetch');
const { ApolloClient } = require('apollo-boost');
const { createHttpLink } = require('apollo-link-http');
const { InMemoryCache } = require('apollo-cache-inmemory');
const { onError } = require('apollo-link-error');
const { ApolloLink } = require('apollo-link');

const errorLink = onError(({ graphQLErrors, networkError }) => {
	if (graphQLErrors) {
		graphQLErrors.forEach(({ message, locations, path }) =>
			console.log(
				`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
			)
		);
	}
	if (networkError) console.log(`[Network error]: ${networkError}`);
});

const HASURA_ENDPOINT = process.env.HASURA_ENDPOINT || 'http://ocean.dev.bsmch.net';
const HASURA_GRAPHQL_ADMIN_SECRET =
	process.env.HASURA_GRAPHQL_ADMIN_SECRET || 'devteam-password-1234';

const httpLink = createHttpLink({
	uri: `${HASURA_ENDPOINT}/v1/graphql`,
	fetch,
	headers: {
		'content-type': 'application/json',
		'x-hasura-admin-secret': HASURA_GRAPHQL_ADMIN_SECRET
	}
});

const client = new ApolloClient({
	link: ApolloLink.from([errorLink, httpLink]),
	fetch,
	cache: new InMemoryCache(),
	defaultOptions: {
		watchQuery: {
			fetchPolicy: 'no-cache',
			errorPolicy: 'ignore'
		},
		query: {
			fetchPolicy: 'no-cache',
			errorPolicy: 'all'
		}
	}
});

exports.client = client;
