const { gql } = require('apollo-boost');

const getStudentsListByCourseId = gql`
	query($course_id: Int!) {
		students: api_all_students(
			where: {
				assignments: {
					course_id: { _eq: $course_id }
					_and: { course: { end_date: { _gte: "now()" } } }
				}
			}
		) {
			first_name
			last_name
			student_number
			assignments(where: { course: { end_date: { _gte: "now()" } } }) {
				user_role_id
				course {
					name
					id
				}
			}
		}
	}
`;
exports.getStudentsListByCourseId = getStudentsListByCourseId;

const getStudentByStudentNumberAndCourseId = gql`
	query($course_id: Int!, $student_number: String!) {
		students: api_all_students(
			where: {
				assignments: {
					course_id: { _eq: $course_id }
					_and: {
						student: { student_number: { _eq: $student_number } }
						_and: { course: { end_date: { _gte: "now()" } } }
					}
				}
			}
		) {
			first_name
			last_name
			student_number
			assignments(where: { course: { end_date: { _gte: "now()" } } }) {
				user_role_id
				course {
					name
					id
				}
			}
		}
	}
`;
exports.getStudentByStudentNumberAndCourseId = getStudentByStudentNumberAndCourseId;
