const { gql } = require('apollo-boost');

const deleteStudentGrades = gql`
	mutation($milestone_ids: [Int!]!, $student_id: Int!) {
		delete_eval_inserted_grades(
			where: { milestone_id: { _in: $milestone_ids }, student_id: { _eq: $student_id } }
		) {
			affected_rows
		}
	}
`;
exports.deleteStudentGrades = deleteStudentGrades;

const insertAndUpdeteGradesArray = gql`
	mutation($grades: [eval_inserted_grades_insert_input!]!) {
		insert_eval_inserted_grades(
			objects: $grades
			on_conflict: {
				constraint: inserted_grades_student_id_milestone_id_author_id_key
				update_columns: [grade]
			}
		) {
			returning {
				id
				grade
				milestone_id
				student_id
				author_id
			}
		}
	}
`;
exports.insertAndUpdeteGradesArray = insertAndUpdeteGradesArray;

const insertGradeAndDescription = gql`
	mutation(
		$studentId: Int!
		$milestoneId: Int!
		$grade: float8!
		$description: String!
		$authorId: Int!
	) {
		insert_fasteval_grades_descriptions_one(
			object: {
				inserted_grade: {
					data: {
						grade: $grade
						milestone_id: $milestoneId
						student_id: $studentId
						author_id: $authorId
					}
				}
				description: $description
			}
		) {
			id
			description
			grade: inserted_grade {
				id
				grade
				author_id
			}
		}
	}
`;
exports.insertGradeAndDescription = insertGradeAndDescription;
