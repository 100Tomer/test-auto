const { gql } = require('apollo-boost');

const getRunCasesByMeeting = gql`
	query($meetingId: Int!) {
		runCases: fasteval_milestones_run_cases(
			where: { milestone: { parent: { _eq: $meetingId } } }
		) {
			id
			run_case
			milestone {
				id
				name
			}
		}
	}
`;
exports.getRunCasesByMeeting = getRunCasesByMeeting;

const insertUpdeteGradeDescriptionByGradeId = gql`
	mutation($gradeId: Int!, $description: String!) {
		gradeDescription: insert_fasteval_grades_descriptions_one(
			object: { description: $description, grade_id: $gradeId }
			on_conflict: {
				constraint: grades_descriptions_grade_id_key
				update_columns: [description]
			}
		) {
			id
			description
			inserted_grade {
				id
				grade
			}
		}
	}
`;
exports.insertUpdeteGradeDescriptionByGradeId = insertUpdeteGradeDescriptionByGradeId;
