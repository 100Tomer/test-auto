const { client } = require('../Common/apollo-client');
const {
	getStudentsListByCourseId,
	getStudentByStudentNumberAndCourseId
} = require('../Queries/StudentsQueries');

const studentsListByCourse = async course_id => {
	const {
		data: { students }
	} = await client.query({
		query: getStudentsListByCourseId,
		variables: {
			course_id
		}
	});

	return students;
};
exports.studentsListByCourse = studentsListByCourse;

const studentByStudentNumberAndCourseId = async (course_id, student_number) => {
	const {
		data: { students }
	} = await client.query({
		query: getStudentByStudentNumberAndCourseId,
		variables: {
			course_id,
			student_number
		}
	});

	return students[0];
};
exports.studentByStudentNumberAndCourseId = studentByStudentNumberAndCourseId;
