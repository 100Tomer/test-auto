const { client } = require('../Common/apollo-client');
const {
	getRunCasesByMeeting,
	insertUpdeteGradeDescriptionByGradeId
} = require('../Queries/RunCaseQueries');

const runCaseByMeeting = async meetingId => {
	const {
		data: { runCases }
	} = await client.query({
		query: getRunCasesByMeeting,
		variables: {
			meetingId
		}
	});

	return runCases;
};
exports.runCaseByMeeting = runCaseByMeeting;

const insertGradeDescriptionByGrade = async (gradeId, description) => {
	const {
		data: { gradeDescription }
	} = await client.query({
		query: insertUpdeteGradeDescriptionByGradeId,
		variables: {
			gradeId,
			description
		}
	});

	return gradeDescription;
};
exports.insertGradeDescriptionByGrade = insertGradeDescriptionByGrade;
