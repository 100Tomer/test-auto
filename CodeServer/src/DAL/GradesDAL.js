const { client } = require('../Common/apollo-client');
const {
	insertAndUpdeteGradesArray,
	deleteStudentGrades,
	insertGradeAndDescription
} = require('../Queries/GradesQueries');

const deleteOldAuthorGrades = async grades => {
	// get the distinct student ids
	const studentIds = grades
		.map(grade => grade.student_id)
		.filter((value, index, self) => self.indexOf(value) === index);

	// get the distinct criterion ids
	const criterions = grades
		.map(grade => grade.milestone_id)
		.filter((value, index, self) => self.indexOf(value) === index);

	await deleteGradesArray(criterions, studentIds);
};
exports.deleteOldAuthorGrades = deleteOldAuthorGrades;

const deleteGradesArray = async (criterions, studentIds) => {
	for (const studentId of studentIds) {
		await deleteStudentGradesByCriterions(criterions, studentId);
	}
};
exports.deleteGradesArray = deleteGradesArray;

const deleteStudentGradesByCriterions = async (milestone_ids, student_id) => {
	await client.mutate({
		mutation: deleteStudentGrades,
		variables: {
			milestone_ids,
			student_id
		}
	});
};
exports.deleteStudentGradesByCriterions = deleteStudentGradesByCriterions;

const insertUpdeteGradesArray = async grades => {
	await client.mutate({
		mutation: insertAndUpdeteGradesArray,
		variables: { grades }
	});
};
exports.insertUpdeteGradesArray = insertUpdeteGradesArray;

const insertOneGradeAndDescription = async (
	studentId,
	milestoneId,
	grade,
	description,
	authorId
) => {
	await client.mutate({
		mutation: insertGradeAndDescription,
		variables: { studentId, milestoneId, grade, description, authorId }
	});
};
exports.insertOneGradeAndDescription = insertOneGradeAndDescription;

const insertGradeAndDescriptionList = async grades => {
	await deleteOldAuthorGrades(grades);

	let promises = [];

	grades.forEach(grade => {
		promises.push(
			insertOneGradeAndDescription(
				grade.student_id,
				grade.milestone_id,
				grade.grade,
				grade.description,
				grade.author_id
			)
		);
	});

	return Promise.all(promises);
};
exports.insertGradeAndDescriptionList = insertGradeAndDescriptionList;
