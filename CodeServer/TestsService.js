const codeTesterManager = require('./codeTester/codeTesterManager');

const runMutipleTests = async (tests) => {
  let results = [];
  let waitingTests = tests.length;
  console.log('waitingTests: ', waitingTests);
  for await (let test of tests) {
    if (test.testCode && test.language && test.code) {
      let currResult = await codeTesterManager.testCode(test.code, test.testCode, test.language, null);
      waitingTests -= 1;
      if(!!test.studentNumber) {
        currResult[0].studentNumber = test.studentNumber;
      }
      await results.push(currResult[0]);
      console.log('Code Testser Results: ' + results);
      console.log('waitingTests: ', waitingTests);
    } else {
      results.push('There was an error with this result!');
    }
  }

  return results;
};

exports.runMutipleTests = runMutipleTests;
