let JavaScriptTemplate =
    `
describe("Generated Test", function() { 
   ? 
});`


const JavaScriptTestTemplate =
    `
    it(?, function() {
        Test.assertEquals(?!, ?`

const getFileText = (testsRequest) => {
    let AllFile = JavaScriptTemplate
    let allTests = ""

    testsRequest.forEach(currentTestRequest => {
        let inputOfFunction = currentTestRequest.inputOfFunction

        if (!(inputOfFunction instanceof Array) && isJson(inputOfFunction)) {
            inputOfFunction = JSON.parse(inputOfFunction)
        }
        console.log(currentTestRequest)
        let functionOutput = currentTestRequest.functionOutput
        console.log(functionOutput)
        let testName = currentTestRequest.testName
        let functionName = currentTestRequest.functionName

        let test = JavaScriptTestTemplate

        test = test.replace("?", "\"" + testName + "\"");

        test = test.replace("?", functionName)

        if (inputOfFunction != null && inputOfFunction != "") {
            // Create Funciton Args
            let functionCall = "("
            inputOfFunction.forEach(current => {
                functionCall += current + ", "
            })
            functionCall = functionCall.substring(0, functionCall.length - 2);
            functionCall += ")"

            test = test.replace("!", functionCall)
        } else {
            test = test.replace("!", "()")
        }

        test = test.replaceAt(test.lastIndexOf("?"), functionOutput + "); \n    });");
        allTests += test;
    });
    AllFile = AllFile.replace("?", allTests)
    return AllFile
}

module.exports = getFileText

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}