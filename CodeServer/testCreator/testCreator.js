const JavaCreator = require('./JavaCreator')
const CppCreator = require('./CppCreator')
const JavaScriptCreator = require('./JavaScriptCreator')
const PythonCreator = require('./PythonCreator')
const languageCreator = {
    "java": JavaCreator,
    "cpp": CppCreator,
    "javascript": JavaScriptCreator,
    "python": PythonCreator,
}

// postman example 
// {
//     "language": "java",
//     "testsRequests": [
//         {
//             "inputOfFunction": ,
//             "functionOutput": "5",
//             "testName": "CheckIfShapeIsRectangle",
//             "functionName": "CreateRectangle"
//         },
//         {
//             "inputOfFunction": ["2332"],
//             "functionOutput": "true",
//             "testName": "CircleTest",
//             "functionName": "IsShapeIsCircle"
//         }
//     ]
// }

const createTestCode = (testsRequest, language) => {
    console.log("Got new request to genereate test code");

    if (languageCreator[language] != null) {

        testRequests = testsRequest.map(currentTestRequest => {
            let functionOutput = currentTestRequest.functionOutput

            let inputOfFunction = currentTestRequest.inputOfFunction;


            if (currentTestRequest.inputOfFunction.includes("[") && currentTestRequest.inputOfFunction.includes("]")) {
                let copyOfInput = currentTestRequest.inputOfFunction
                console.log(copyOfInput)
                let allInputs = copyOfInput.replace("[", "").replace("]", "").split(",")
                console.log(allInputs)
                allInputs = allInputs.map(current => {
                    console.log(current)
                    let onlyNumber = current.replace("'", "").replace("'", "").replace("\"", "").replace("\"", "");
                    console.log(onlyNumber)
                    console.log(testIfInputIsRandomizeInput(onlyNumber))
                    if (testIfInputIsRandomizeInput(onlyNumber)) {
                        let IsFirstNumberNegative = false

                        if (onlyNumber[0] == "+" || onlyNumber[0] == "-") {
                            if (onlyNumber[0] == "-") {
                                IsFirstNumberNegative = true;
                                onlyNumber = onlyNumber.substring(1);
                            }
                        }
                        let firstNumber = parseFloat(onlyNumber.split("-")[0])
                        if (IsFirstNumberNegative) {
                            firstNumber *= -1
                        }



                        let splitMiddleDash = onlyNumber.split("-")
                        let leftSecondAfterSplit = splitMiddleDash[splitMiddleDash.length - 1]
                        let isSecondNegative = false
                        if (splitMiddleDash[splitMiddleDash.length - 2] == "") {

                            isSecondNegative = true
                        }
                        let secondNumber = parseFloat(leftSecondAfterSplit)
                        if (isSecondNegative) {
                            secondNumber *= -1
                        }

                        console.log(firstNumber)
                        console.log(secondNumber)

                        let randomNumber = firstNumber

                        if (isInt(firstNumber) && isInt(secondNumber)) {
                            randomNumber = Math.floor(Math.random() * (secondNumber - firstNumber + 1) + firstNumber);
                        } else {
                            randomNumber = Math.random() * (secondNumber - firstNumber) + firstNumber;
                        }

                        return randomNumber.toString();
                    } else {
                        return current
                    }
                });
                console.log(allInputs)
                if (allInputs.length > 1) {
                    inputOfFunction = "[" + allInputs.join(",") + "]";
                } else {
                    inputOfFunction = allInputs;
                }
            }



            // if (functionOutput[0] != "\"" && functionOutput[0] != "'") {
            //     functionOutput = "\"" + functionOutput;
            // }
            // if (functionOutput[functionOutput.length - 1] != "\"" && functionOutput[functionOutput.length - 1] != "'") {
            //     functionOutput = functionOutput + "\"";
            // }

            return {
                "functionOutput": functionOutput,
                "testName": currentTestRequest.testName,
                "functionName": currentTestRequest.functionName,
                "inputOfFunction": inputOfFunction,
            }

        })

        // TODO: need to send this four variables to the creators. 
        let testCode = languageCreator[language](testRequests);
        return testCode;
    }

    return "";
}

exports.createTestCode = createTestCode


const testIfInputIsRandomizeInput = (input) => {
    const regex = /[-+]{0,1}[\d]*[-]{1}[-+]{0,1}[\d]*/g;

    let m;

    if ((m = regex.exec(input)) !== null) {
        if (m.length > 0) {
            return true
        }

    }
    return false
}

function isInt(n) {
    return n % 1 === 0;
}