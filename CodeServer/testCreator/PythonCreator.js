let PythonTemplate =
    `test.describe("Generated Test")
?`


const PythonTestTemplate =
    `test.it(?)
test.assert_equals(?!, ?)`

const getFileText = (testsRequest) => {
    let AllFile = PythonTemplate
    let allTests = ""

    testsRequest.forEach(currentTestRequest => {
        let inputOfFunction = currentTestRequest.inputOfFunction

        if (!(inputOfFunction instanceof Array) && isJson(inputOfFunction)) {
            inputOfFunction = JSON.parse(inputOfFunction)
        }

        let functionOutput = currentTestRequest.functionOutput
        let testName = currentTestRequest.testName
        let functionName = currentTestRequest.functionName

        let test = PythonTestTemplate

        test = test.replace("?", "\"" + testName + "\"");

        test = test.replace("?", functionName)

        if (inputOfFunction != null && inputOfFunction != "") {
            // Create Funciton Args
            let functionCall = "("
            inputOfFunction.forEach(current => {
                functionCall += current + ", "
            })
            functionCall = functionCall.substring(0, functionCall.length - 2);
            functionCall += ")"

            test = test.replace("!", functionCall)
        } else {
            test = test.replace("!", "()")
        }
        test = test.replaceAt(test.lastIndexOf("?"), functionOutput + ") \n");
        allTests += test;
    });
    AllFile = AllFile.replace("?", allTests)
    return AllFile
}

module.exports = getFileText

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}