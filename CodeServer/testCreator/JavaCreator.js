const javaTemplate =
    `import org.junit.Test;
import static org.junit.Assert.*;


public class MyTests {
    ?

}
`
// ! is the function arg brackets 
// ? is instade
const javaTestTemplate =
    `
@Test
public void ?() {
    ?(Main.?!, ?`


const getFileText = (testsRequest) => {
    let AllFile = javaTemplate
    let allTests = ""

    testsRequest.forEach(currentTestRequest => {
        let inputOfFunction = currentTestRequest.inputOfFunction

        if (!(inputOfFunction instanceof Array) && isJson(inputOfFunction)) {
            inputOfFunction = JSON.parse(inputOfFunction)
        }

        let functionOutput = currentTestRequest.functionOutput
        let testName = currentTestRequest.testName
        let functionName = currentTestRequest.functionName

        let test = javaTestTemplate
        test = test.replace("?", testName.replace(/ /g, ''))

        // check wich assertEquals
        if (isNaN(functionOutput) && functionOutput.startsWith("[") && functionOutput.endsWith("]")) {
            test = test.replace("?", "assertArrayEquals")
        } else {
            test = test.replace("?", "assertEquals")
        }

        test = test.replace("?", functionName)

        if (inputOfFunction != null && inputOfFunction != "") {
            // Create Funciton Args
            let functionCall = "("
            inputOfFunction.forEach(current => {
                functionCall += current + ", "
            })
            functionCall = functionCall.substring(0, functionCall.length - 2);
            functionCall += ")"

            test = test.replace("!", functionCall)
        } else {
            test = test.replace("!", "()")
        }
        test = test.replaceAt(test.lastIndexOf("?"), functionOutput + "); \n}");
        allTests += test;
    });
    AllFile = AllFile.replace("?", allTests)

    return AllFile
}
String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}

module.exports = getFileText

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}