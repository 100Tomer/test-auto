// run docker on if it is down
console.log("System OS: " + process.platform);

let currentOS = process.platform;
const OS = {
    Aix: 'aix',
    Darwin: 'darwin', // Mac OS
    Freebsd: 'freebsd',
    Linux: 'Linux', // Linux
    Openbsd: 'openbsd',
    Sunos: 'sunos',
    Win32: 'win32', // Windows
};

const runDocker = () => {
    // Open docker on mac os
    if (currentOS == OS.Darwin) {
        execShellCommand("open -a docker");
    }
}

exports.runDocker = runDocker


async function execShellCommand(cmd) {
    const exec = require('child_process').exec;
    return new Promise((resolve, reject) => {
        exec(cmd, (error, stdout, stderr) => {
            if (error) {
                console.warn(error);
                resolve("Execute Error - " + error);

            } else if (stderr && !stderr.includes("Unhandled rejection TestError:") && !stderr.includes("AssertException")) {
                resolve("Syntax Error - " + stderr);
            } else {
                resolve(stdout ? stdout : stderr);
            }
        });
    });
}