const fs = require('fs');
const CodeAnalyzerResult = require('./models/CodeAnalyzerResult')
const commandExecuter = require("../commandExecuter")

//  Delete the tmp library when startedv
const analizableLanguages = ["java"]

const rulesToIgnore = ["NoPackage"]

const analizeCode = async (folderName, fileName, language) => {
    if (isLanguageHasAnalyzer(language)) {

        let filePath = folderName + "/" + fileName;
        return new Promise(async (resolve, reject) => {

            let command = getCommandFromLanguage(language, fileName, folderName);
            let output = await commandExecuter.execShellCommand(command, [4]);
            let codeAnalyzerResults = parseInputIntoCodeAnalyzerResults(output);
            resolve(codeAnalyzerResults);


        });
    } else {
        return new Promise((resolve) => {
            resolve([])
        })
    }
}

exports.analizeCode = analizeCode


// This is the output structure is csv: 
// "Problem","Package","File","Priority","Line","Description","Rule set","Rule"
// "1","","/Users/tomercohen/Documents/React/RunningClient/codeservice/CodeServer/codeAnalizeSample-LqEnPZHixEzjYv77KlEP/HelloWorld.java","3","1","All classes, interfaces, enums and annotations must belong to a named package","Code Style","NoPackage"
const parseInputIntoCodeAnalyzerResults = (output) => {
    let splitedIntoLines = output.split("\n")

    if (splitedIntoLines.length > 1) {

        // remove the first item (cvs healines)
        splitedIntoLines.shift()
        splitedIntoLines = splitedIntoLines.filter(current => current != '')

        let totalCodeAnalyzerResults = [];
        for (problemCase of splitedIntoLines) {
            let dataLineCells = problemCase.split(",");
            let duplicateLine = replaceAll(dataLineCells[4], "\"", "");
            let description = replaceAll(dataLineCells[dataLineCells.length - 3], "\"", "");
            let problemRule = replaceAll(dataLineCells[dataLineCells.length - 1], "\"", "");
            let UXProblemRule = problemRule.replace(/([A-Z])/g, ' $1').trim();
            if (!rulesToIgnore.includes(problemRule)) {
                totalCodeAnalyzerResults.push(new CodeAnalyzerResult(parseInt(duplicateLine), parseInt(duplicateLine), description, UXProblemRule));
            }

        }

        return totalCodeAnalyzerResults
    } else {
        return []
    }
}


const isLanguageHasAnalyzer = (language) => {

    return analizableLanguages.includes(language);
}

const getCommandFromLanguage = (language, fileName, executeFolder) => {
    const command = "$HOME/pmd-bin-6.28.0/bin/run.sh pmd -d ./" + executeFolder + " -f csv -R rulesets/java/quickstart.xml -no-cache"
    return command
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}