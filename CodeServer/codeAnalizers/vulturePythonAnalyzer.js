const fs = require('fs');
const CodeAnalyzerResult = require('./models/CodeAnalyzerResult')
const commandExecuter = require("../commandExecuter")
const analizableLanguages = ["python"]

const analizeCode = async (folderName, fileName, language) => {
    if (isLanguageHasAnalyzer(language)) {

        let filePath = folderName + "/" + fileName;
        return new Promise(async (resolve, reject) => {

            let command = getCommandFromLanguage(language, fileName, folderName);
            let output = await commandExecuter.execShellCommand(command, [1]);
            let codeAnalyzerResults = parseInputIntoCodeAnalyzerResults(output);
            resolve(codeAnalyzerResults);


        });
    } else {
        return new Promise((resolve) => {
            resolve([])
        })
    }
}

exports.analizeCode = analizeCode

const parseInputIntoCodeAnalyzerResults = (output) => {
    if (!output.includes('Execute Error')) {
        let splitedIntoLines = output.split("\n")
        if (splitedIntoLines.length > 0) {

            splitedIntoLines = splitedIntoLines.filter(current => current != '')
            let totalCodeAnalyzerResults = [];
            console.log(splitedIntoLines)
            for (problemCase of splitedIntoLines) {

                let dataLineCells = problemCase.split(":")
                if (dataLineCells.length >= 3 && dataLineCells[1] != null) {

                    let problemLine = dataLineCells[1]
                    let description = dataLineCells[2]
                    let problemRule = "Hint Problem"
                    totalCodeAnalyzerResults.push(new CodeAnalyzerResult(parseInt(problemLine), parseInt(problemLine), description, problemRule));
                }
            }

            return totalCodeAnalyzerResults
        } else {
            return []
        }
    }
}

const isLanguageHasAnalyzer = (language) => {

    return analizableLanguages.includes(language);
}

const getCommandFromLanguage = (language, fileName, executeFolder) => {

    const command = "vulture ./" + executeFolder + "/" + fileName;
    return command
}