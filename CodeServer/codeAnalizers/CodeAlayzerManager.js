const fs = require('fs');
//const FacebookInfer = require('./Facebook-Infer')
const DuplicateDetector = require('./DuplicateDetector')
const PmdInfer = require('./Pmd-Infer')
const JSHint = require('./JSHint')
const SecurityAnalyzer = require('./SecurityAnalyzer')
//const vulturePythonAnalyzer = require('./vulturePythonAnalyzer')
const whiteSpacesAnalyzer = require('./whiteSpacesAnalyzer')
const CoalaAnylzer = require('./CoalaAnylzer')
const CodeAnalyzerResult = require('./models/CodeAnalyzerResult')

const languagesEnding = {
    "java": ".java",
    "cpp": ".cpp",
    "swift": ".swift",
    "javascript": ".js",
    "python": ".py"
}

//const codeAnalyzersFileBased = [FacebookInfer.analizeCode, DuplicateDetector.analizeCode, PmdInfer.analizeCode, JSHint.analizeCode, SecurityAnalyzer.analizeCode, vulturePythonAnalyzer.analizeCode, CoalaAnylzer.analizeCode]
const codeAnalyzersFileBased = [DuplicateDetector.analizeCode, PmdInfer.analizeCode, JSHint.analizeCode, CoalaAnylzer.analizeCode]
const codeAnalyzersCodeBased = [whiteSpacesAnalyzer.analizeCode]

const AnalyzeCode = async (code, language) => {

    let codeClassName = getClassNameFromCode(code, language)
    let fileName = codeClassName + languagesEnding[language];
    if (codeClassName == "") {
        fileName = "RandomFileName-" + generateId(20) + languagesEnding[language];;
    }
    let folderName = "codeAnalizeSample-" + generateId(20);
    let filePath = folderName + "/" + fileName;
    console.log("##################################  Code Analyze Started  ##################################")
    console.log("Analize Path: " + filePath);
    console.log("File Name :" + fileName);

    return new Promise(async (resolve, reject) => {
        fs.mkdir("./" + folderName, {
            recursive: true
        }, (err) => {
            if (err) {
                resolve([])
            } else {

                fs.writeFile(filePath, code, async (err) => {

                    try {

                        let allCodeAnalyzerResults = []

                        const finishAllAnalyzers = () => {
                            deleteFolderRecursive("./" + folderName)
                            resolve(allCodeAnalyzerResults)
                        }

                        let numnerOfAnalyzerFinished = 0
                        codeAnalyzersFileBased.forEach(async (analyzer) => {
                            let analyzerOutput = await analyzer(folderName, fileName, language)
                            if (analyzerOutput instanceof Array) {
                                allCodeAnalyzerResults.push(...analyzerOutput)
                            }
                            numnerOfAnalyzerFinished += 1;
                            if (numnerOfAnalyzerFinished == codeAnalyzersFileBased.length && numnerOfAnalyzerCodeBasedFinished == codeAnalyzersCodeBased.length) {
                                finishAllAnalyzers()
                            }
                        })

                        let numnerOfAnalyzerCodeBasedFinished = 0
                        codeAnalyzersCodeBased.forEach(async (analyzer) => {
                            let analyzerOutput = await analyzer(code, language)
                            if (analyzerOutput instanceof Array) {
                                allCodeAnalyzerResults.push(...analyzerOutput)
                            }
                            numnerOfAnalyzerCodeBasedFinished += 1;
                            if (numnerOfAnalyzerCodeBasedFinished == codeAnalyzersCodeBased.length && numnerOfAnalyzerFinished == codeAnalyzersFileBased.length) {
                                finishAllAnalyzers()
                            }
                        })
                    } catch (error) {
                        console.error("Got Error in analysis " + error)
                        deleteFolderRecursive("./" + folderName)
                        resolve([])

                    }
                })
            }
        })
    })
}


exports.AnalyzeCode = AnalyzeCode


const generateId = (length) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


const deleteFolderRecursive = function (path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

const getClassNameFromCode = (code, language) => {
    if (language == "java" || language == "cpp") {
        if (code.includes("class")) {
            const wordsArray = code.split(" ");
            let className = wordsArray[wordsArray.indexOf("class") + 1];
            className = className.replace("{", "");
            return className
        }
    }
    return ""
}