const fs = require('fs');
const CodeAnalyzerResult = require('./models/CodeAnalyzerResult')
const commandExecuter = require("../commandExecuter")

//  Delete the tmp library when startedv
const analizableLanguages = ["java", "cpp", "swift", "python", "ruby"]

const commandLanguagesString = {

};

const analizeCode = async (folderName, fileName, language) => {
    if (isLanguageHasAnalyzer(language)) {

        let filePath = folderName + "/" + fileName;
        return new Promise(async (resolve, reject) => {

            let command = getCommandFromLanguage(language, fileName, folderName);
            let output = await commandExecuter.execShellCommand(command, [4]);
            let codeAnalyzerResults = parseInputIntoCodeAnalyzerResults(output);
            resolve(codeAnalyzerResults);


        });
    } else {
        return new Promise((resolve) => {
            resolve([])
        })
    }
}

exports.analizeCode = analizeCode


// This is the output structure is csv: 
// lines,tokens,occurrences
// 9,19,2,2,/Users/tomercohen/Documents/React/RunningClient/codeservice/CodeServer/./codeAnalizeSample-xEkF8vBOozdEvKsiJhQt/HelloWorld.java,12 
const parseInputIntoCodeAnalyzerResults = (output) => {
    let splitedIntoLines = output.split("\n")

    if (splitedIntoLines.length > 1) {

        // remove the first item (cvs healines)
        splitedIntoLines.shift()
        splitedIntoLines = splitedIntoLines.filter(current => current != '')

        let totalCodeAnalyzerResults = [];

        for (duplicteCase of splitedIntoLines) {
            let dataLineCells = duplicteCase.split(",")
            let afterFilePathRemoved = dataLineCells.filter((current) => {
                return (parseInt(current) == current)
            })

            let duplicateLinesLength = afterFilePathRemoved[0];
            let numberOfDuplicates = afterFilePathRemoved[2];
            for (let duplicatedIndex = 0; duplicatedIndex < numberOfDuplicates; duplicatedIndex += 1) {
                let duplicatedStartLine = afterFilePathRemoved[duplicatedIndex + 3];

                totalCodeAnalyzerResults.push(new CodeAnalyzerResult(parseInt(duplicatedStartLine), parseInt(duplicatedStartLine) + parseInt(duplicateLinesLength) - 2, "Duplicated Code - Occurrence: " + (duplicatedIndex + 1), "Duplicated Code"));
            }
        }

        return totalCodeAnalyzerResults
    } else {
        return []
    }
}


const isLanguageHasAnalyzer = (language) => {

    return analizableLanguages.includes(language);
}

const getCommandFromLanguage = (language, fileName, executeFolder) => {
    let commandLanguage = language
    if (commandLanguagesString[language] != null) {
        commandLanguage = commandLanguagesString[language]
    }
    const command = "$HOME/pmd-bin-6.28.0/bin/run.sh cpd --minimum-tokens 40 --files ./" + executeFolder + " --language " + commandLanguage + " --format csv"
    return command
}