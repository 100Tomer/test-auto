const fs = require('fs');
const CodeAnalyzerResult = require('./models/CodeAnalyzerResult')

//  Can add anything that want the "basmch stadarts"
const analizableLanguages = ["java", "javascript", "cpp", "python"]


const analizeCode = async (code, language) => {
    if (isLanguageHasAnalyzer(language)) {

        return new Promise(async (resolve, reject) => {
            let allCodeAnalyzerResults = [];
            allCodeAnalyzerResults.push(...checkUnneededEmptyLine(code));
            allCodeAnalyzerResults.push(...checkVar(code, language));
            resolve(allCodeAnalyzerResults);
        });
    } else {
        return new Promise((resolve) => {
            resolve([])
        })
    }
}

exports.analizeCode = analizeCode

const checkVar = (code, language) => {
    let codeAnalyzerResults = [];
    if (language == "javascript") {

        let codeByLines = code.split("\n");
        let lineNumber = 1;

        const regex = /^[^"'\n\/]*var {1}[a-zA-Z\d]* *=/mg;

        codeByLines.forEach(line => {
            while ((m = regex.exec(line)) !== null) {

                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }


                m.forEach(() => {
                    codeAnalyzerResults.push(new CodeAnalyzerResult(lineNumber, lineNumber, "'var' should be replaced with 'let'", "Using var"));
                });
            }
            lineNumber += 1;
        });
    }
    return codeAnalyzerResults
}

const checkUnneededEmptyLine = (code) => {
    let codeByLines = code.split("\n");
    let isLastLineIsEmptyLine = false;
    let lineNumber = 1;
    let codeAnalyzerResults = [];
    codeByLines.forEach(line => {
        if (line == "" || !line.replace(/\s/g, '').length) {
            if (isLastLineIsEmptyLine) {
                codeAnalyzerResults.push(new CodeAnalyzerResult(lineNumber, lineNumber, "This line is unnecessary line", "Unnecessary Line"));
                isLastLineIsEmptyLine = false;
            } else {
                isLastLineIsEmptyLine = true;
            }
        } else {
            isLastLineIsEmptyLine = false;
        }
        lineNumber += 1;
    });

    return codeAnalyzerResults
}

const isLanguageHasAnalyzer = (language) => {

    return analizableLanguages.includes(language);
}