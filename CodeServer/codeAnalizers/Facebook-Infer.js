const fs = require('fs');
const CodeAnalyzerResult = require('./models/CodeAnalyzerResult')
const commandExecuter = require("../commandExecuter")

//  Delete the tmp library when startedv
const analizableLanguages = ["java", "cpp"]

const folderOfAnalizeFiles = "./tmp/out"

const analizeCode = async (folderName, fileName, language) => {
    if (isLanguageHasAnalyzer(language)) {

        let filePath = folderName + "/" + fileName;
        return new Promise(async (resolve, reject) => {

            let command = getCommandFromLanguage(language, fileName, folderName);
            let output = await commandExecuter.execShellCommand(command);

            console.log(output)

            try {
                let rawdata = fs.readFileSync(folderName + "/tmp/out/report.json");
                let outputArray = JSON.parse(rawdata);

                let codeAnalyzerResults = []

                codeAnalyzerResults = outputArray.map(currentCodeError => {
                    return new CodeAnalyzerResult(currentCodeError["line"], currentCodeError["line"], currentCodeError["qualifier"], currentCodeError["bug_type"])
                })
                resolve(codeAnalyzerResults)
            } catch (error) {
                console.error("Facebook infer error")
            } finally {
                resolve([])
            }


        });
    } else {
        return new Promise((resolve) => {
            resolve([])
        })
    }
}

exports.analizeCode = analizeCode



const isLanguageHasAnalyzer = (language) => {

    return analizableLanguages.includes(language);
}

const getCommandFromLanguage = (language, fileName, executeFolder) => {
    const baseCommand = "cd " + executeFolder + "&& infer run  --eradicate -o " + folderOfAnalizeFiles;
    const javaCommand = baseCommand + " -- javac " + fileName
    const cppCommand = baseCommand + " -- gcc -c " + fileName;

    if (language == "java") {
        return javaCommand
    }
    if (language == "cpp") {
        return cppCommand
    }

    return ""
}