const fs = require('fs');
const CodeAnalyzerResult = require('./models/CodeAnalyzerResult')
const commandExecuter = require("../commandExecuter")

//  Not the full list.. Go To https://github.com/wireghoul/graudit for full list.
const analizableLanguages = ["javascript", "java", "python", "c", "actionscript", "ruby", "php", "perl"]

const analizableLanguagesNaming = {
    "javascript": "js"
}

const analizeCode = async (folderName, fileName, language) => {
    if (isLanguageHasAnalyzer(language)) {

        let filePath = folderName + "/" + fileName;
        return new Promise(async (resolve, reject) => {

            let command = getCommandFromLanguage(language, fileName, folderName);
            let output = await commandExecuter.execShellCommand(command);
            let codeAnalyzerResults = parseInputIntoCodeAnalyzerResults(output);
            resolve(codeAnalyzerResults);


        });
    } else {
        return new Promise((resolve) => {
            resolve([])
        })
    }
}

exports.analizeCode = analizeCode

const parseInputIntoCodeAnalyzerResults = (output) => {
    if (!output.includes('Execute Error')) {
        let splitedIntoLines = output.split("\n")
        if (splitedIntoLines.length > 0) {

            splitedIntoLines = splitedIntoLines.filter(current => current != '')
            let totalCodeAnalyzerResults = [];
            for (problemCase of splitedIntoLines) {

                let dataLineCells = problemCase.split(":")
                if (dataLineCells.length >= 3 && dataLineCells[1] != null) {

                    let problemLine = dataLineCells[1]
                    let description = "This command can be a security threat. Use It carefully"
                    let problemRule = "Security Tight Location"
                    totalCodeAnalyzerResults.push(new CodeAnalyzerResult(parseInt(problemLine), parseInt(problemLine), description, problemRule));
                }
            }

            return totalCodeAnalyzerResults
        } else {
            return []
        }
    }
}

const isLanguageHasAnalyzer = (language) => {

    return analizableLanguages.includes(language);
}

const getCommandFromLanguage = (language, fileName, executeFolder) => {
    let languageInCommand = language
    if (analizableLanguagesNaming[language] != null) {
        languageInCommand = analizableLanguagesNaming[language]
    }
    const command = "./SecurityCheckFrameWork/graudit -d " +
        languageInCommand + " -c 0 -z -B -A ./" + executeFolder;
    return command
}