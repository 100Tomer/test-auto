const fs = require('fs');
const CodeAnalyzerResult = require('./models/CodeAnalyzerResult')
const commandExecuter = require("../commandExecuter")

//  Delete the tmp library when startedv
const analizableLanguages = ["javascript"]

const analizeCode = async (folderName, fileName, language) => {
    if (isLanguageHasAnalyzer(language)) {

        return new Promise(async (resolve, reject) => {

            let command = getCommandFromLanguage(language, fileName, folderName);
            let output = await commandExecuter.execShellCommand(command, [2]);
            let codeAnalyzerResults = parseInputIntoCodeAnalyzerResults(output);
            resolve(codeAnalyzerResults);


        });
    } else {
        return new Promise((resolve) => {
            resolve([])
        })
    }
}

exports.analizeCode = analizeCode


// This is the output structure:
// server.js: line 31, col 2, Missing semicolon.
const parseInputIntoCodeAnalyzerResults = (output) => {
    console.log(output)
    let splitedIntoLines = output.split("\n")

    if (splitedIntoLines.length > 1) {

        splitedIntoLines = splitedIntoLines.filter(current => current != '')
        splitedIntoLines.pop()
        let totalCodeAnalyzerResults = [];

        for (problemCase of splitedIntoLines) {



            let dataLineCells = problemCase.replace(":", ",").split(", ")

            let problemLine = replaceAll(dataLineCells[1], "line ", "");
            let description = dataLineCells[3]
            let problemRule = "Hint Problem"

            totalCodeAnalyzerResults.push(new CodeAnalyzerResult(parseInt(problemLine), parseInt(problemLine), description, problemRule));

        }

        return totalCodeAnalyzerResults
    } else {
        return []
    }
}

const isLanguageHasAnalyzer = (language) => {

    return analizableLanguages.includes(language);
}

const getCommandFromLanguage = (language, fileName, executeFolder) => {
    const command = "jshint ./" + executeFolder + "/" + fileName + " --config \"jsHintConf.json\"";
    return command
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}