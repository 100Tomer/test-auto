const fs = require('fs');
const CodeAnalyzerResult = require('./models/CodeAnalyzerResult')
const commandExecuter = require("../commandExecuter")

//  Delete the tmp library when startedv
// java is not includede cause i already import all thier bears manually like "pmd infer"
const analizableLanguages = ["javascript", "cpp", "python"]

const languagesBears = {
    "javascript": ["JSComplexityBear"],
    "cpp": ["CSecurityBear", "CPPCheckBear"],
    "python": ["PyUnusedCodeBear", "BanditBear"]
}

const analizeCode = async (folderName, fileName, language) => {
    if (isLanguageHasAnalyzer(language)) {

        return new Promise(async (resolve, reject) => {

            let command = getCommandFromLanguage(language, fileName, folderName);
            let output = await commandExecuter.execShellCommand(command, [1]);
            let codeAnalyzerResults = parseInputIntoCodeAnalyzerResults(output);
            resolve(codeAnalyzerResults);
        });
    } else {
        return new Promise((resolve) => {
            resolve([])
        })
    }
}

exports.analizeCode = analizeCode


// This is the output structure:
// {
//     "results": {
//       "cli": [
//         {
//           "additional_info": "",
//           "affected_code": [
//             {
//               "end": {
//                 "column": null,
//                 "file": "/app/RandomFileName-CGzPWgnRPDnL2u8ojgku.cpp",
//                 "line": 5
//               },
//               "file": "/app/RandomFileName-CGzPWgnRPDnL2u8ojgku.cpp",
//               "start": {
//                 "column": null,
//                 "file": "/app/RandomFileName-CGzPWgnRPDnL2u8ojgku.cpp",
//                 "line": 3
//               }
//             }
//           ],
//           "aspect": "NoneType",
//           "confidence": 100,
//           "debug_msg": "",
//           "diffs": {
//             "/app/RandomFileName-CGzPWgnRPDnL2u8ojgku.cpp": "--- \n+++ \n@@ -1,5 +1,7 @@\n #include <string>\n \n-std::string greet() { \n-  int a = 5; return \"hello world!\"; \n-}+std::string greet ()\n+{\n+    int a = 5;\n+    return \"hello world!\";\n+}\n"
//           },
//           "id": 52840359101382219655763812881669475320,
//           "message": "Indentation can be improved.",
//           "message_arguments": {},
//           "message_base": "Indentation can be improved.",
//           "origin": "GNUIndentBear",
//           "severity": 1
//         }
//       ]
//     }
//   }
const parseInputIntoCodeAnalyzerResults = (output) => {
    console.log(output)
    output = cleanEscapeCauseByTerminal(output)

    try {

        let jsonOutput = JSON.parse(output);
        return jsonOutput["results"]["cli"].map(result => {
            let startLine = result["affected_code"][0]["start"]["line"];
            let endLine = result["affected_code"][0]["end"]["line"];
            let discription = result["message_base"] + " (" + "confidence: " + result["confidence"] + "%)";
            // let discription = Object.values(result["diffs"])[0] + "confidence: " + result["confidence"]
            let title = result["message_base"]

            return new CodeAnalyzerResult(startLine, endLine, discription, title)
        })
    } catch (e) {
        console.error(e);
        return []
    }

}

// apreadntly the string ends with invisable: \u001b[m0
const cleanEscapeCauseByTerminal = (code) => {
    if (code.includes("\u001b")) {
        return code.substring(0, code.indexOf("\u001b"));
    }
    return code
}


const isLanguageHasAnalyzer = (language) => {

    return analizableLanguages.includes(language);
}

const getCommandFromLanguage = (language, fileName, executeFolder) => {
    const bearsToCommand = languagesBears[language].join(",");
    const command = "cd " + executeFolder + " && docker run -t -v $(pwd):/app --workdir=/app coala/base coala --ci -I --files " + fileName + " --bears " + bearsToCommand + "  --disable-caching --json";
    return command
}