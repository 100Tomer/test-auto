//  CodeAnalyzerResult: 
//  Start Line - 60
//  End Line - 70 or may be the same as 60
//  Type - Like null problem
//  Description - the variable num can be null....
//  
module.exports = class CodeAnalyzerResult {

    constructor(startLine, endLine, description, type) {
        this.startLine = startLine;
        this.endLine = endLine;
        this.description = description;
        this.type = type;
    }


    toString() {
        return "\n" +
            "Start Line: " + this.startLine + "\n" +
            "End Line: " + this.endLine + "\n" +
            "Description: " + this.description + "\n" +
            "Type: " + this.type + "\n";
    }
}