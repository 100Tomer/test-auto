// command, int array, string array
execShellCommand = async (cmd, errorCodeToIgnore, errorStringsToIgnore) => {
    const exec = require('child_process').exec;
    return new Promise((resolve, reject) => {
        exec(cmd, (error, stdout, stderr) => {
            console.log(stdout)
            if (error && (errorCodeToIgnore == null || !errorCodeToIgnore.includes(error["code"]))) {
                console.warn("Execute Error: " + error);
                resolve("Execute Error - " + error);

            } else {

                let isErrorNeedShouldIgnored = false;
                if (errorStringsToIgnore != null) {
                    errorStringsToIgnore.forEach(stringToIgnore => {
                        if (stderr.includes(stringToIgnore)) {
                            isErrorNeedShouldIgnored = true;

                        }
                    });
                }

                if (stderr && !isErrorNeedShouldIgnored) {
                    resolve("Syntax Error - " + stderr);
                } else {
                    resolve(stdout ? stdout : stderr);
                }
            }
        });
    });
}


exports.execShellCommand = execShellCommand