const axios = require('axios');
const fs = require("fs");
const { readdirSync } = require("fs");
const path = require("path");

const fetchFiles = async (major, course, subject, tar, version) => {
  console.info(`get files of tar ${tar} of course ${course} version ${version}`);
  const url = `${process.env.FILES_SERVER_IP}/files/byCourse/${encodeURIComponent(major)}/${course}/${subject}/${tar}/${version}`;
  let requesedFiles;
  try {
  console.log(url);
    requesedFiles = await (await axios.get(url)).data;
  } catch (err) {
    requesedFiles = {};
  }

  return requesedFiles;
};

const fetchFilesByFileName = async (major, course, subject, tar, version, fileName) => {
  console.info(`get files of tar ${tar} of course ${course} version ${version} and basename ${fileName}`);
  const url = `${process.env.FILES_SERVER_IP}/files/byBaseName/${encodeURIComponent(major)}/${course}/${subject}/${tar}/${version}/${fileName}`;
  let requesedFiles;
  try {
    console.log(url);
    requesedFiles = await (await axios.get(url)).data;
  } catch (err) {
    requesedFiles = {};
  }

  return requesedFiles;
};

 const fetchLocalFiles = async (major, course, subject, tar) => {
      let requestedFiles = {};
      try {
      console.log(`get all files of ${course} in ${subject}- ${tar}`);
      console.log("course: " + course);
      console.log("subject: " + subject);
      console.log("tar: " + tar);
      console.log("major: " + major);
      const filePath = `../../Files/${major}/${course}/${subject}/${tar}`;
      console.log(filePath);
      const fileEndings = getDirectories(filePath);
      let codesToSend = {};

      await fileEndings.forEach((end) => {
        let currentFolder = `${filePath}/${end}/`;
        readdirSync(currentFolder).forEach((file) => {
          codesToSend[file] = fs.readFileSync(
            path.resolve(`${currentFolder}${file}`),
            "utf8"
          );
        });
      });
      requestedFiles = codesToSend;
    } catch (e) {
      console.log("The error in files function: " +e); 
      console.log("error in getFiles function");
    }
    
    return requestedFiles
};

const getDirectories = (source) =>
  readdirSync(source, { withFileTypes: true })
    .filter((dirent) => dirent.isDirectory())
    .map((dirent) => dirent.name);

exports.fetchFiles = fetchFiles;
exports.fetchLocalFiles = fetchLocalFiles;
exports.fetchFilesByFileName = fetchFilesByFileName;

