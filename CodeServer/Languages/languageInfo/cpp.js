module.exports = {
    "language": "cpp",
    "Included Libraries": [],
    "Already Loaded Libraries": [],
    "Load Libraries Explanation": "",
    "Load Libraries Example": "",
    "Installed Services": [],
    "Test Timeout": "12 seconds",
    "codeExample": {
        "Code": "#include <string>\n\nstd::string greet() { \n  return \"hello world!\"; \n}",
        "TestCode": "Describe(HelloWorld) { \n  It(GreetWorld) { \n     Assert::That(greet(), Equals(\"hello world!\")); \n } \n};"
    },
    "dockerRunner": "codewars/systems-runner",
    "testRunner": "cw",
    "clientLanguageIcon": "language-cpp", // Client will add "mdi-" in the beginning see icons list https://materialdesignicons.com/
    "languageVersions": ["14"],
}