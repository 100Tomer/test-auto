module.exports = {
    "language": "dart",
    "Included Libraries": [],
    "Already Loaded Libraries": [],
    "Load Libraries Explanation": "",
    "Load Libraries Example": "",
    "Installed Services": [],
    "Test Timeout": "12 seconds ? ",
    "Best Run Time": "150",
    "codeExample": {
        "Code": "greet() => 'hello world!';",
        "TestCode": "test('Return the correct string', () {\n     expect(greet(), equals('hello world!'));\n});"
    },
    "dockerRunner": "codewars/dart-runner",
    "testRunner": "test",
    "clientLanguageIcon": "tablet-cellphone", // Client will add "mdi-" in the beginning see icons list https://materialdesignicons.com/
    "languageVersions": ["1.23.0"],
}