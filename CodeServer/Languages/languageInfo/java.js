module.exports = {
    "language": "java",
    "Included Libraries": ["joda-time 2.2", "guava 20.0", "commons-lang3 3.6", "commons-math3 3.6.1", "jsoup 1.10.3", "dom4j 2.0.1", "assertj-guava 3.1.0", "hibernate-core 5.2.10.Final", "mongo-java-driver 3.4.2", "sqlite-jdbc 3.19.3", "postgresql 42.1.1", "spring-boot-starter-web 1.5.4", "spring-boot-starter-data-mongodb 1.5.4", "spring-boot-starter-data-redis 1.5.4", "spring-boot-starter-data-jpa 1.5.4", "spring-boot-starter-data-rest 1.5.4", "spring-boot-starter-validation 1.5.4"],
    "Already Loaded Libraries": ["junit 4.12", "lombok 1.16.18", "mockito-core 2.7.19", "assertj-core 3.8.0"],
    "Load Libraries Explanation": "`@config reference` statements",
    "Load Libraries Example": "java\n// @config: reference guava\n// @config: reference commons-lang3",
    "Installed Services": [],
    "Test Timeout": "20 seconds",
    "Best Run Time": "7500",
    "codeExample": {
        "Code": "public class HelloWorld { \n  public static String greet() { \n    return \"hello world!\"; \n  } \n}",
        "TestCode": "import org.junit.Test; \n import static org.junit.Assert.assertEquals; \n \n public class HelloWorldTest { \n @Test \n public void testHelloWorld() throws Exception {  \n  assertEquals(\"hello world!\", HelloWorld.greet()); \n }\n}"
    },
    "dockerRunner": "codewars/java-runner",
    "testRunner": "cw",
    "clientLanguageIcon": "language-java", // Client will add "mdi-" in the beginning see icons list https://materialdesignicons.com/
    "languageVersions": ["1.8.0_91"],
}