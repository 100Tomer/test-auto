module.exports = {
    "language": "swift",
    "Included Libraries": [],
    "Already Loaded Libraries": [],
    "Load Libraries Explanation": "",
    "Load Libraries Example": "",
    "Installed Services": [],
    "Test Timeout": "",
    "codeExample": {
        "Code": "func greet() -> String{\n  return \"hello world!\" \n}",
        "TestCode": "import XCTest\n\nclass SolutionTest: XCTestCase {\n    static var allTests = [\n        (\"Making sure greet exists\", testExample),\n    ]\n\n    func testExample() {\n        XCTAssertEqual(greet(),\"hello world!\")\n    }\n}\n\nXCTMain([\n    testCase(SolutionTest.allTests)\n])"
    },
    "dockerRunner": "codewars/swift-runner",
    "testRunner": "cw",
    "clientLanguageIcon": "language-swift", // Client will add "mdi-" in the beginning see icons list https://materialdesignicons.com/
    "languageVersions": ["3.1.1"],
}