module.exports = {
    "language": "python",
    "Included Libraries": ["scikit-learn", "tensorflow", "numpy", "scipy", "jsoup 1.10.3", "pandas", "pymongo", "redis"],
    "Already Loaded Libraries": [],
    "Load Libraries Explanation": "",
    "Load Libraries Example": "",
    "Installed Services": ["sqlite", "redis-server", "mongodb"],
    "Test Timeout": "12 seconds",
    "Best Run Time": "83",
    "codeExample": {
        "Code": "def greet():\n    return \"hello world!\"",
        "TestCode": "test.describe(\"Greet function\")\ntest.it(\"Making sure greet exists\")\ntest.assert_equals(greet(), \"hello world!\", \"Greet doesn't return hello world!\")"
    },
    "dockerRunner": "codewars/python-runner",
    "testRunner": "cw",
    "clientLanguageIcon": "language-python", // Client will add "mdi-" in the beginning see icons list https://materialdesignicons.com/
    "languageVersions": ["2.x", "3.x"]
}