module.exports = {
    "language": "javascript",
    "Included Libraries": ["async", "baconjs", "base64-js", "bluebird", "brain", "brfs", "buffer-equal", "chai", "chai-change", "chai-factories", "chai-spies", "chai-subset", "dclassify", "deku", "elasticsearch", "esprima", "expect", "falafel", "graph-paths ", "immutable", "js-yaml", "karma", "karma-chai", "karma-mocha", "karma-phantomjs-launcher", "karma-typescript", "karma-typescript-angular2-transform", "karma-typescript-es6-transform", "lodash", "mocha", "mongodb", "mongoose", "natural", "phantomjs", "quickcheck", "react", "react-dom", "redis", "rx", "should", "sqlite3"],
    "Already Loaded Libraries": [],
    "Load Libraries Explanation": "",
    "Load Libraries Example": "",
    "Installed Services": ["sqlite", "redis-server", "mongodb"],
    "Test Timeout": "12 seconds",
    "Best Run Time": "713",
    "codeExample": {
        "Code": "function greet() { \n return \"hello world!\"; \n }",
        "TestCode": "describe(\"Testing function\", function() { \n   it(\"Is it a function?\", function() {\n        Test.assertEquals(typeof greet, \"function\", \"greet should be a function\"); \n   });\n   it(\"Correct return-value?\", function() { \n      Test.assertEquals(greet(), \"hello world!\");\n   });\n});"
    },
    "dockerRunner": "codewars/node-runner",
    "testRunner": "cw",
    "clientLanguageIcon": "language-javascript", // Client will add "mdi-" in the beginning see icons list https://materialdesignicons.com/
    "languageVersions": ["0.10.33", "0.10.33/Babel", "6.11.0", "6.11.0/Babel", "8.1.3", "8.1.3/Babel"],
}