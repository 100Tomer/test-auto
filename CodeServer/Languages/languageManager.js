const SwiftData = require('./languageInfo/swift');
const PythonData = require('./languageInfo/python');
const JavaData = require('./languageInfo/java');
const CppData = require('./languageInfo/cpp');
const JavaScriptData = require('./languageInfo/javascript');
const DartData = require('./languageInfo/dart');

// Swift currenly not included cause of test problem
const allLanguages = [JavaScriptData, PythonData, JavaData, CppData, DartData]


const getDataOfLangauge = (langauge) => {
    let copyOfAllLanguages = allLanguages.map(a => ({
        ...a
    }));
    const filterdLangaugesData = copyOfAllLanguages.filter(current => current.language == langauge)
    if (filterdLangaugesData.length == 0) {
        throw "Could not find information for " + langauge;
    } else {
        return copyOfAllLanguages.filter(current => current.language == langauge)[0]
    }
}

const allRunners = () => {

    return allLanguages.map(currentLangauge => currentLangauge.dockerRunner)
}

const RunnerOf = (langauge) => {

    return getDataOfLangauge(langauge).dockerRunner
}

const codeExampleOf = (langauge) => {

    return getDataOfLangauge(langauge).codeExample
}

const allLanguagesIconsOf = () => {

    let allLanguagesIcons = {};

    allLanguages.forEach(currentLangauge => {
        allLanguagesIcons[currentLangauge.language.toLowerCase()] = currentLangauge.clientLanguageIcon
    })

    return allLanguagesIcons;
}

const languageVersionsOf = (langauge) => {

    return getDataOfLangauge(langauge).languageVersions
}

const languageTestRuunerOf = (langauge) => {

    return getDataOfLangauge(langauge).testRunner
}

const AllLanguageVersions = () => {
    const data = {}
    console.log(allLanguages)
    allLanguages.forEach(current => {
        data[current.language] = current.languageVersions;
    })
    return data
}

module.exports = {
    allRunners: allRunners,
    codeExampleOf: codeExampleOf,
    getDataOfLangauge: getDataOfLangauge,
    languageVersionsOf: languageVersionsOf,
    RunnerOf: RunnerOf,
    AllLanguageVersions: AllLanguageVersions,
    languageTestRuunerOf: languageTestRuunerOf,
    allLanguagesIconsOf: allLanguagesIconsOf
}