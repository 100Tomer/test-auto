import java.util.Scanner;

public class Main {

    private static final Scanner scanner = new Scanner(System.in);
    private static final int CALCULATE = 0;
    private static final int QUIT = 1;
    private static final int MIN_BASE = 2;
    private static final int MAX_BASE = 10;

    private static void printMenu() {
        System.out.println(
                "=====================================================\n" +
                        "Base Calculator Menu:\n" +
                        "*********************\n" +
                        "Enter " + CALCULATE + " to perform a calculation\n" +
                        "Enter " + QUIT + " to exit the bases calculator\n" +
                        "=====================================================");
    }

    private static boolean isBaseValid(int baseNumber) {
        if ((baseNumber < MIN_BASE) ||
                (baseNumber > MAX_BASE)) {


            return false;
        } else {
            return true;
        }
    }

    private static boolean isNumInBase(int numberToChange, int numBase) {
        boolean isNumValid = true;
        int inputNumberTemp = numberToChange;

        while ((inputNumberTemp != 0) && isNumValid) {
            int firstDigit = inputNumberTemp % 10;
            
            if (firstDigit >= numBase) {
                isNumValid = false;

            } else {
                inputNumberTemp /= 10;
            }
        }
        return isNumValid;
    }

    private static int convertNumberToDecimal(int numberToChange, int numBase) {
        if (numBase == 10) {
            return numberToChange;
        } else {
            int digitValue = 1;
            int decimalNum = 0;
            int inputNumberTemp = numberToChange;
            
            while ((inputNumberTemp != 0)) {
                int firstDigit = inputNumberTemp % 10;
                decimalNum += firstDigit * digitValue;
                digitValue *= numBase;
                inputNumberTemp /= 10;
            }
            
            return decimalNum;
        }
    }

    private static int convertDecimalToNumber(int decimalNum, int outputNumBase) {
        // Convert the decimal number to the base
        // specified by the user:
        int outputNum = 0;
        int digitValue = 1;

        // While there are digits left
        while (decimalNum != 0) {
            outputNum += (decimalNum % outputNumBase) *
                    digitValue;
            digitValue *= 10;
            decimalNum /= outputNumBase;
        }

        return outputNum;
    }

    public static String calc(int[] inputs) {
        printMenu();
        int inputIndex = 0;
        String finalResult = "";

        int userChoice = inputs[inputIndex];
        inputIndex++;

        int decimalNum = 0;
        int result;

        while (userChoice != QUIT) {
            System.out.print("Enter the source number: ");
            int inputNumber = inputs[inputIndex];
            inputIndex++;

            if (inputNumber < 0) {
                System.out.println("Invalid input: The number to convert must be positive");
                finalResult += "Invalid input: The number to convert must be positive ";
            } else {
                System.out.println("Enter the base of the source number " +
                        "(between " + MIN_BASE + " and " + MAX_BASE + "): ");
                int inputNumBase = inputs[inputIndex];
                inputIndex++;

                if (isBaseValid(inputNumBase)) {
                    if (isNumInBase(inputNumber, inputNumBase)) {
                        decimalNum = convertNumberToDecimal(inputNumber, inputNumBase);
                        System.out.println("Enter the destination base number (between " + MIN_BASE + " and " + MAX_BASE + ")");
                        int outputNumBase = inputs[inputIndex];
                        inputIndex++;

                        if (isBaseValid(outputNumBase)) {
                            if (outputNumBase == inputNumBase) {
                                result = inputNumber;
                            } else if (outputNumBase == 10) {
                                result = decimalNum;
                            } else {
                                result = convertDecimalToNumber(decimalNum, outputNumBase);
                            }

                            // Print the result:
                            System.out.println("The result is: " + result + " (in base "+ outputNumBase + ")");
                            finalResult += "The result is: " + result + " (in base " + outputNumBase + ") ";
                        } else {
                            System.out.println("Invalid input: The base " + outputNumBase
                                    + " is not between " + MIN_BASE + " and " + MAX_BASE);
                            finalResult += "Invalid input: The base " + outputNumBase + " is not between " + MIN_BASE + " to " + MAX_BASE+" ";
                        }
                    } else {
                        System.out.println("Invalid input: The number " + inputNumber +
                                " is illegal in base: " + inputNumBase);
                        finalResult += "Invalid input: The number " + inputNumber + " is illegal in base: " + inputNumBase+" ";
                    }
                } else {
                    System.out.println("Invalid input: The base " + inputNumBase + " is not between " + MIN_BASE + " to" + MAX_BASE);
                    finalResult += "Invalid input: The base " + inputNumBase + " is not between " + MIN_BASE + " to " + MAX_BASE+" ";
                }
            }

            printMenu();
            userChoice = inputs[inputIndex];
            inputIndex++;
        }

        return finalResult;
    }


    public static void main(String[] args) {
        printMenu();

        int userChoice = scanner.nextInt();
        int decimalNum = 0;
        int result;

        while (userChoice != QUIT) {
            System.out.print("Enter the source number: ");
            int inputNumber = scanner.nextInt();

            if (inputNumber < 0) {
                System.out.println("Invalid input: The number to convert must be positive");
            } else {
                System.out.println("Enter the base of the source number " +
                        "(between " + MIN_BASE + " and " + MAX_BASE + "): ");
                int inputNumBase = scanner.nextInt();

                if (isBaseValid(inputNumBase)) {
                    if (isNumInBase(inputNumber, inputNumBase)) {
                        decimalNum = convertNumberToDecimal(inputNumber, inputNumBase);
                        System.out.println("Enter the destination base number (between " + MIN_BASE + " and " + MAX_BASE + ")");
                        int outputNumBase = scanner.nextInt();

                        if (isBaseValid(outputNumBase)) {
                            if (outputNumBase == inputNumBase) {
                                result = inputNumber;
                            } else if (outputNumBase == 10) {
                                result = decimalNum;
                            } else {
                                result = convertDecimalToNumber(decimalNum, outputNumBase);
                            }
                            
                            // Print the result:
                            System.out.println("The result is: " + result + " (in base "
                                    + outputNumBase + ")");
                        }
                    }
                }
            }

            printMenu();
            userChoice = scanner.nextInt();
        }
    }
}