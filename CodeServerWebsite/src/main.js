import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import DocumentationPage from "./components/documentationPage/DocumentationPage";
import RunTestsPage from "./components/RunTestsPage";
import CodePage from "./components/CodePage";
// import Page404 from "./components/Page404/Page404"
import ErrorPage from "./components/ErrorPage";
import VueRouter from "vue-router";
import VueClipboard from "vue-clipboard2";
import VueAlertify from "vue-alertify";

Vue.use(VueClipboard);
Vue.use(VueRouter);
Vue.use(VueAlertify, {});

Vue.config.productionTip = false;

const routes = [
	{
		path: '/',
		component: CodePage
	},
	{
		path: '/Run-tests',
		component: RunTestsPage
	},
	{
		path: '/Documentation',
		component: DocumentationPage
	},
	{
		path: '/Error',
		name: 'Error',
		component: ErrorPage,
		props: true
	},
	{
		path: '/404',
		component: ErrorPage,
		props: {
			message: "The page you're looking\nfor can't be found",
			buttonText: 'Go back to home screen'
		}
	},
	{
		path: '*',
		redirect: '/404'
	}
];

const router = new VueRouter({
  routes,
  mode: "history",
  history: true,
});

new Vue({
  vuetify,
  router,
  render: (h) => h(App),
}).$mount("#app");
