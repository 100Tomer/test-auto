import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import VuePrismEditor from "vue-prism-editor";
import "vue-prism-editor/dist/VuePrismEditor.css"; // import the styles
import '@fortawesome/fontawesome-free/css/all.css'
Vue.component("prism-editor", VuePrismEditor);
Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdi', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
    },
    theme: {
        dark: true,
    },
});