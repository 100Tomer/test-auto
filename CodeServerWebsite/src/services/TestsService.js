
import ExcelJS from 'exceljs'
import axios from 'axios';
import { saveAs } from 'file-saver';

/* eslint-disable no-debugger */
export class TestsService {
  static async runTests (code, language, testCode, languageVersion) {
  console.info(`send tests to server`);
  let raw = [{
    code: code,
    language: language.toLowerCase(),
    testCode: testCode,
    languageVersion: languageVersion
  }];

  return axios.post(`${process.env.VUE_APP_SERVER_IP}/`, raw);
}

 static async runTestsByTestAndCourse (major, course, subject, tar, version, language) {
  const url = `${process.env.VUE_APP_SERVER_IP}/run/multiple/byCourse/${major}/${course}/${subject}/${tar}/${version}/${language}`;
  return axios.get(url);
}

	static async runLocalTestsByTestAndCourse(path) {
		const url = `${process.env.VUE_APP_SERVER_IP}/run/local/multiple/byCourse/${path}`;
		return axios.get(url);
	}

static async exportTestsReultsToExcel (results, major, subject, tar, course, cases) {
  const workbook = new ExcelJS.Workbook();
  results.forEach(result => {
    if(result.testResults) {
      result.testResults.sort(function(a,b){
        if(a.testDescription < b.testDescription){
          return -1;
        }
        
        if(a.testDescription > b.testDescription) {
          return 1;
        }
        
        return 0;
      });
    }
  });

  workbook.views = [
    {
      x: 0, y: 0, width: 10000, height: 20000,
      firstSheet: 0, activeTab: 1, visibility: 'visible'
    }
  ];

  const sheet = workbook.addWorksheet(`results-${course}-${tar}`,{
    properties:{tabColor:{argb:'33E3FF'}}
  });

  let rows = [];
  rows = this.generateRows(results,cases);

  sheet.addRows(rows);
  sheet.views = [{ rightToLeft: true }];

  for (let index = 1; index <= (results.length + 1); index++) {
    sheet.getColumn(index).eachCell((cell,rownum) => {
      if(rownum < rows.length - 1){
        if (cell.value === true) {
          cell.value = 1;
        } else if (cell.value === false) {
          cell.value = 0;
        }
      }
    });
  }
  for (let colIndex = 2; colIndex < (results.length + 2); colIndex++) {
    let currResult = results[colIndex - 2];
    sheet.getColumn(colIndex).eachCell((cell,rownum)=>{
      if(rownum > 1 && rownum < (rows.length - 2) &&
      currResult.testResults &&
         !currResult.testResults[rownum - 2].isTestPassed) {
           let promptTitle = '';
           let promptMessage = '';

           if(currResult.testResults[rownum - 2].isRuntimeError) {
            promptTitle = `Exception Details`;
            promptMessage = currResult.testResults[rownum - 2].exceptionDetails;
           } else {
              promptTitle = `actual: ${currResult.testResults[rownum - 2].actual}`;
              promptMessage = `expected: ${currResult.testResults[rownum - 2].expected}`;
           }
        
           cell.dataValidation = {
          showInputMessage: true,
          promptTitle: promptTitle,
          prompt:  promptMessage
        }
      }
    });

    if(results[colIndex - 2].syntaxErrorDescription) {
      sheet.getCell(rows.length,colIndex).note = {
        texts:[
          {'font': {'size': 12, 'bold':true}, 'text': 'syntax error description: '},
          {'font': {'size': 12}, 'text': results[colIndex - 2].syntaxErrorDescription},
        ]
      };
    }
    
  }
  
  this.saveSheet(workbook);
}

 static async saveSheet (workbook) {
    const buf = await workbook.xlsx.writeBuffer();
    saveAs(new Blob([buf]), 'results.xlsx');
 }

 static generateCorrectResultsRow(rows,results) {
    let rowToAdd = [];
    rowToAdd.push('correct:');
    let correctResultsCounter;
    for (let colIndex = 1; colIndex < (results.length + 1); colIndex++) {
      correctResultsCounter = 0;
      for (let rowIndex = 1; rowIndex < rows.length; rowIndex++) {
        if(rows[rowIndex][colIndex]) {
          correctResultsCounter++;
        }
      }

      rowToAdd.push(correctResultsCounter);
    }

    return rowToAdd;
  }

  static generateRows(results, cases) {
    let rows = [];

    let headerRow = ['תיאור המקרה'];
    results.forEach(result => {
      headerRow.push(result.studentNumber);
    });
    rows.push(headerRow);
    let rowToAdd = [];
    for (let caseIndex = 0; caseIndex < cases.length; caseIndex++) {
      rowToAdd = [];
      rowToAdd.push(cases[caseIndex]);
      for (let studentIndex = 0; studentIndex < results.length; studentIndex++) {
        results[studentIndex].testResults ? rowToAdd.push(results[studentIndex].testResults[caseIndex].isTestPassed) :rowToAdd.push(false) ;
      }
      rows.push(rowToAdd);
    }

    rowToAdd = this.generateCorrectResultsRow(rows,results);
    rows.push(rowToAdd);

    let executeErrorRow = [];
    let syntaxErrorRow = [];

    executeErrorRow.push('Execute error: ');
    syntaxErrorRow.push('Syntax error: ');
    
    results.forEach(result => {
      executeErrorRow.push(result.isExecuteError);
      result.isSyntaxError? syntaxErrorRow.push(result.isSyntaxError) : syntaxErrorRow.push(false);
    });

    rows.push(executeErrorRow);
    rows.push(syntaxErrorRow);
    return rows;
  }

  static generateCols(results) {
    let columns = [{ name: 'תיאור המקרה', filterButton: true,width: 10 }];
    results.forEach(result => {
      columns.push({name:result.studentNumber, filterButton: false, width:10 });
    });

    return columns;
  }
}
/* eslint-enable no-debugger */
